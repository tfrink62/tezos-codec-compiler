open Abstract_type
open Foreign_ast

module Gen : sig
  module Context : sig
    module S : sig
      type key = Qualification.t

      type 'a t
    end

    type t = Qualification.t S.t

    val fresh : unit -> t

    (** Find the proper qualification, including base_name as prefix, of a given
        raw identifier *)
    val lookup : Qualification.t -> t -> Qualification.t

    (** Clear the definitions of locally defined ad-hoc types for re-use in
        other modules *)
    val reset_local : t -> t
  end
end

(** Generate a namespace-wrapped set of definitions of a toplevel type and all
    of its named internal types, returning an updated version of the parameter
    [ctxt].

    @param name Raw identifier used for the toplevel type
    @param ctxt Generator context from previous passes
    @param rep [typrep] representing the toplevel type
    @return Tuple consisting of the updated context and the namespace-wrapped
    definitions *)
val convert_toplevel :
  name:string ->
  ctxt:Gen.Context.t ->
  rep:typrep ->
  (string * typrep) list ->
  Gen.Context.t * (binding_mods * declaration) list
