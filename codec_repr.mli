(** [size_repr]: possible representations for the byte-length prefix of a
    dynamic field *)
type size_repr = [ `Uint8 | `Uint30 ] [@@deriving sexp]

type tag_size = [ `Uint8 | `Uint16 ] [@@deriving sexp]

type enum_size = [ size_repr | tag_size ] [@@deriving sexp]

(** [width]: Representation of the byte-width of variable-width types *)
type width =
  | DynPrefix of size_repr
      (** Explicit value-level width, included in the encoding as a
          length-prefix of the specified kind *)
  | ReserveSuffix of int
      (** Implicit type-level width, imputed from the constant total byte-width
          of all subsequent fields in the enclosing structure *)
[@@deriving sexp]

type subtype_desc = { title : string; _description : string option }
[@@deriving sexp]

type datakind_spec = VariableKind | DynamicKind | FloatKind of int
[@@deriving sexp]

module Int_size : sig
  type t = Uint8 | Uint16 | Uint32 | Uint64 | Int8 | Int16 | Int32 | Int64
  [@@deriving sexp]

  exception Invalid_int_size of string

  val of_string_opt : string -> t option

  (** [to_size_repr s] converts the string [s] into the [Int_size.t] variant it
      is a stringification of.

      @param s String to be converted
      @raise Invalid_int_size When [s] does not correspond to the
      stringification of an [Int_size.t] *)
  val of_string_exn : string -> t

  (** Attempts to coerce an [Int_size.t] to its matching [size_repr] variant,
      ignoring sub-byte bit-length differences *)
  val to_size_repr : t -> size_repr option

  (** Attempts to coerce an [Int_size.t] to its matching [tag_size] variant *)
  val to_tag_size : t -> tag_size option
end

type int_size = Int_size.t [@@deriving sexp]

(** Structural encoding of the binary layout of a schema type *)
module Layout : sig
  (** Subtype consisting of all non-recursive layout encodings *)
  type t' =
    [ `Zero_width
    | `Int of int_size
    | `Bool
    | `RangedInt of int * int
    | `RangedFloat of float * float
    | `Float
    | `Bytes
    | `String
    | `Enum of int_size * string
    | `Ref of string
    | `Padding ]
  [@@deriving sexp]

  (** Type consisting of all layout encodings, up to a recursive depth of 1 *)
  type t = [ `Seq of t' | t' ] [@@deriving sexp]

  (** Coerce a non-[`Seq] variant of [t] into the narrowed type [t']

      @raise [Assert_failure] when called on [`Seq _] *)
  val narrow : t -> t'

  (** Coerce any variant of [t'] into the widened type [t] *)
  val widen : t' -> t
end

type layout_spec = Layout.t [@@deriving sexp]

(** Type used to represent a tagged schema case-variant with its numeric tag,
    possibly absent name, and corresponding fields *)
type tagtype_repr = {
  tag : int;
  fields : field_spec list;
  name : string option;
}
[@@deriving sexp]

and type_encoding =
  | Tagged of {
      tag_size : tag_size;
      kind : datakind_spec;
      cases : tagtype_repr list;
    }
      (** Algebraic data-type, represented by tag-size and variant-size metadata
          and a list of case-variants *)
  | Fielded of { fields : field_spec list }
      (** Simple record-like type, represented by a list of fields *)
  | Enumerated of { size : enum_size; cases : (string * int) list }
      (** Pure enum-type, represented by an [enum_size] and a list of
          [(name,value)] pairs *)
[@@deriving sexp]

and subtype_spec = { desc : subtype_desc; encoding : type_encoding }
[@@deriving sexp]

and field_spec =
  | DynField of { name : string option; num_fields : int; width : size_repr }
      (** 'Dynamic' pseudo-field that indicates the total byte-width of at least
          one successive field *)
  | NamedField of {
      name : string;
      layout : layout_spec;
      data_kind : datakind_spec;
    }  (** Explicitly named field *)
  | AnonField of { layout : layout_spec; data_kind : datakind_spec }
      (** Explicitly unnamed field *)
  | OptionalField of { name : string }
      (** 'Optional' pseudo-field that indicates the presence or absence of the
          field that immediately follows *)
[@@deriving sexp]

and binary_spec = { toplevel : type_encoding; subtypes : subtype_spec list }
[@@deriving sexp]

and codec_type = {
  ident : string;
  _json : (Json_ast.json_schema[@sexp.opaque]);
  binary : binary_spec;
}
[@@deriving sexp]

and codec_tree = Many of codec_type list | Single of codec_type
[@@deriving sexp]
