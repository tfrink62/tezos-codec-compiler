import { TypedParser } from "./byteparser";
import { Decoder, decoder_argument } from "./decoder";
import { Encoder } from "./encoder";
import { uint16encoder, uint8encoder, uint16decoder, uint8decoder } from "./integral";

/**
 * Any type with a field named '_tag', which we use to distinguish variants of ADTs
 * 
 * Generic parameter U is used to limit tag values to finite collections of natural numbers
 */
export interface Variant<U> {
    _tag: U,
}

/** Byte-width of a variant tag field */
export type tag_size = "uint16" | "uint8"

/** Generic encoder for a [[Variant]] that is
 * partially applied to the tag field's [[width]] specification
 */
type TagEncoder<U, T extends Variant<U>> = (tag_width: tag_size) => Encoder<T>;

/** Generic encoder for a [[Variant]] that is
 * partially applied to the tag field's [[width]] specification
 */
type TagDecoder<U extends number, T extends Variant<U>> = (tag_width: tag_size) => Decoder<T>;

/**
 * Type alias for the non-tag fields of a variant type
 */
export type Tagless<T extends Variant<unknown>> = Omit<T, "_tag">;


/**
 * Takes an encoder responsible for serializing all non-tag fields of a record representing
 * a single variant of an ADT and returns a composite tag-aware encoder parametrized over the
 * byte-width of the tag pseudo-field, which produces the full serialization for that ADT
 * variant (including the appropriately formatted tag-value prefix)
 * 
 * @param enc Encoder responsible for serializing all non-tag fields of the variant record
 * @returns [[TagEncoder]] function responsible for encoding 
 */
export const variant_encoder =
    <U extends number, T extends Variant<U>>(encs: { [key: number]: Encoder<Object> }): TagEncoder<U, T> =>
        (tag_width: tag_size): Encoder<T> =>
            (val: T): string => {
                var tag_prefix: string;
                switch (tag_width) {
                    case "uint16":
                        tag_prefix = uint16encoder(val._tag as number);
                        break;
                    case "uint8":
                        tag_prefix = uint8encoder(val._tag as number);
                        break;
                }
                if (val._tag in encs) {
                    let payload = encs[val._tag](val);
                    return tag_prefix + payload;
                } else {
                    throw new Error(`Encoder table given to variant_encoder has no encoder for valid tag ${val._tag}`);
                }
            }

/**
 * Takes a decoder responsible for parsing, from a hex-string or TypedParser, each of the non-tag
 * fields of a single variant of an ADT, and returns a composite tag-aware decoder parametrized over the
 * byte-width of the tag pseudo-field, which produces the full serialization for that ADT
 * variant (including the appropriately formatted tag-value prefix)
 * 
 * @param enc Encoder responsible for serializing all non-tag fields of the variant record
 * @returns [[TagEncoder]] function responsible for encoding 
 */
export const variant_decoder =
    <U extends number, T extends Variant<U>>(decs: { [key: number]: Decoder<Object> }): TagDecoder<U, T> =>
        (tag_width: tag_size): Decoder<T> =>
            (input: decoder_argument): T => {
                var tag: number;
                var p = TypedParser.parse(input);
                switch (tag_width) {
                    case "uint16":
                        tag = uint16decoder(p);
                        break;
                    case "uint8":
                        tag = uint8decoder(p);
                        break;
                }
                if (tag in decs) {
                    let payload = decs[tag](p);
                    return { _tag: tag as U, ...payload } as T;
                } else {
                    throw new Error(`variant_decoder: Encountered illegal (undefined) variant tag ${tag}`);
                }
            }



