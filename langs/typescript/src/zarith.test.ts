import { Z, N } from './zarith';
import * as fc from 'fast-check';

describe('Z.t tests', () => {
    const cases: Array<[bigint, string]> = [
        [0n, "00"],
        [1n, "01"],
        [-1n, "41"],
        [64n, "8001"],
        [-64n, "c001"],
    ];
    const encoder = Z.t_encoder;
    const decoder = Z.t_decoder;
    const min_val = -0xffff_ffff_ffff_ffffn
    const max_val = 0xffff_ffff_ffff_ffffn
    const bigintbounds = { min: min_val, max: max_val };

    test('encoder', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.bigInt(bigintbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

describe('N.t tests', () => {
    const cases: Array<[bigint, string]> = [
        [0n, "00"],
        [1n, "01"],
        [0x41n, "41"],
        [128n, "8001"],
        [192n, "c001"],
    ];
    const encoder = N.t_encoder;
    const decoder = N.t_decoder;
    const min_val = 0x0n;
    const max_val = 0xffff_ffff_ffff_ffffn;
    const bigintbounds = { min: min_val, max: max_val };

    test('encode', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toBe(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1n)).toThrow();
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.bigInt(bigintbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});