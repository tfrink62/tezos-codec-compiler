import { assertFlowBaseAnnotation } from '@babel/types';
import { Optional, Maybe, OldMaybe, ListMaybe, Stack } from './util';



/** SECTION - Tests for OldMaybe class */
describe('OldMaybe class tests', () => {
    test('is_present correctness', () => {
        let present = new OldMaybe(32);
        let absent = new OldMaybe();
        let of_undef = new OldMaybe(undefined);
        let of_null = new OldMaybe(null);
        expect(present.is_present()).toBe(true);
        expect(absent.is_present()).toBe(false);
        expect(of_undef.is_present()).toBe(false);
        expect(of_null.is_present()).toBe(true);
    });

    test('is_absent correctness', () => {
        let present = new OldMaybe(32);
        let absent = new OldMaybe();
        let of_undef = new OldMaybe(undefined);
        let of_null = new OldMaybe(null);
        expect(present.is_absent()).toBe(false);
        expect(absent.is_absent()).toBe(true);
        expect(of_undef.is_absent()).toBe(true);
        expect(of_null.is_absent()).toBe(false);
    });

    test('extract_value correctness', () => {
        let present = new OldMaybe(32);
        let absent = new OldMaybe();
        let of_undef = new OldMaybe(undefined);
        let of_null = new OldMaybe(null);
        expect(present.extract_value()).toBe(32);
        expect(present.extract_value(42)).toBe(32);
        expect(absent.extract_value(42)).toBe(42);
        expect(() => absent.extract_value()).toThrow();
        expect(of_undef.extract_value(42)).toBe(42);
        expect(() => of_undef.extract_value()).toThrow();
        expect(of_null.extract_value(42)).toBeNull();
        expect(of_null.extract_value()).toBeNull();
    });
});
// !SECTION

/** SECTION - Tests for Maybe class */
describe('Maybe class tests', () => {
    test('is_present correctness', () => {
        let some = Maybe.Some(42);
        let some_null = Maybe.Some(null);
        let some_undef = Maybe.Some(undefined);
        let none = Maybe.None();
        expect(some.is_present()).toBe(true);
        expect(some_null.is_present()).toBe(true);
        expect(some_undef.is_present()).toBe(true);
        expect(none.is_present()).toBe(false);
    });

    test('is_absent correctness', () => {
        let some = Maybe.Some(42);
        let some_null = Maybe.Some(null);
        let some_undef = Maybe.Some(undefined);
        let none = Maybe.None();
        expect(some.is_absent()).toBe(false);
        expect(some_null.is_absent()).toBe(false);
        expect(some_undef.is_absent()).toBe(false);
        expect(none.is_absent()).toBe(true);
    });

    test('extract_value correctness', () => {
        let some = Maybe.Some(32);
        let some_null = Maybe.Some(null);
        let some_undef = Maybe.Some(undefined);
        let none = Maybe.None();
        expect(some.extract_value()).toBe(32);
        expect(some.extract_value(42)).toBe(32);
        expect(some_null.extract_value()).toBeNull();
        expect(some_null.extract_value(42)).toBeNull();
        expect(some_undef.extract_value()).toBeUndefined();
        expect(some_undef.extract_value(42)).toBeUndefined();
        expect(none.extract_value(42)).toBe(42);
        expect(() => none.extract_value()).toThrow();
    });
});
// !SECTION

/** SECTION - Tests for ListMaybe class */
describe('ListMaybe class tests', () => {
    test('is_present correctness', () => {
        let some = ListMaybe.Some(42);
        let some_null = ListMaybe.Some(null);
        let some_undef = ListMaybe.Some(undefined);
        let none = ListMaybe.None();
        expect(some.is_present()).toBe(true);
        expect(some_null.is_present()).toBe(true);
        expect(some_undef.is_present()).toBe(true);
        expect(none.is_present()).toBe(false);
    });

    test('is_absent correctness', () => {
        let some = ListMaybe.Some(42);
        let some_null = ListMaybe.Some(null);
        let some_undef = ListMaybe.Some(undefined);
        let none = ListMaybe.None();
        expect(some.is_absent()).toBe(false);
        expect(some_null.is_absent()).toBe(false);
        expect(some_undef.is_absent()).toBe(false);
        expect(none.is_absent()).toBe(true);
    });

    test('extract_value correctness', () => {
        let some = ListMaybe.Some(32);
        let some_null = ListMaybe.Some(null);
        let some_undef = ListMaybe.Some(undefined);
        let none = ListMaybe.None();
        expect(some.extract_value()).toBe(32);
        expect(some.extract_value(42)).toBe(32);
        expect(some_null.extract_value()).toBeNull();
        expect(some_null.extract_value(42)).toBeNull();
        expect(some_undef.extract_value()).toBeUndefined();
        expect(some_undef.extract_value(42)).toBeUndefined();
        expect(none.extract_value(42)).toBe(42);
        expect(() => none.extract_value()).toThrow();
    });
});
// !SECTION


/** Tests for Stack class */

test('construct from existing array', () => {
    let arr: Array<number> = [1, 2, 3, 4, 5];
    let stack = new Stack(arr);
    expect(stack.size()).toBe(5);
    expect(stack.pop()).toBe(5);
    expect(stack.pop()).toBe(4);
    expect(stack.pop()).toBe(3);
    expect(stack.pop()).toBe(2);
    expect(stack.pop()).toBe(1);
    expect(stack.pop()).toBeUndefined();
});

test('default constructor empty', () => {
    let stack = new Stack<number | string>();
    expect(stack.size()).toBe(0);
    expect(stack.peek()).toBeUndefined()
    expect(stack.pop()).toBeUndefined()
    expect(stack.peek_def(12)).toBe(12);
    expect(stack.peek_def("foo")).toBe("foo");
});

test('push updates size and peek', () => {
    let stack = new Stack<number>();
    for (var i = 0; i < 100; i++) {
        expect(stack.size()).toBe(i);
        stack.push(i);
        expect(stack.peek()).toBe(i);
    }
});

test('pop updates size and peek', () => {
    let stack = new Stack<number>();
    for (var i = 0; i < 100; i++) {
        stack.push(i);
    }

    expect(stack.size()).toBe(100);

    for (var j = 99; j >= 0; j--) {
        expect(stack.pop()).toBe(j);
        expect(stack.size()).toBe(j);
    }

    expect(stack.peek()).toBeUndefined();
    expect(stack.pop()).toBeUndefined();
    expect(stack.size()).toBe(0);
});

test('peek correctness', () => {
    const empty_stack = new Stack<number>();
    const singleton_stack = new Stack<number>([0]);

    for (var i = 1; i < 100; i++) {
        expect(empty_stack.peek_def(i)).toBe(i);
        expect(singleton_stack.peek_def(i)).toBe(0);
    }
});