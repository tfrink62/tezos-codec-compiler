/**
 * Interface representing any immutable value-store that is either empty or a singleton.
 */
export interface Optional<T> {
    /**
     * Indicates whether the value-store is empty
     */
    is_absent(): boolean,
    /**
     * Indicates whether the value-store has a singleton value stored
     */
    is_present(): boolean,
    /**
     * Returns the singleton value contained within the store if there is one,
     * or the default value passed in as a parameter if it is empty.
     * 
     * Throws an error if the store is empty and `def` is `undefined` (either in value, or when not provided)
     * @param def Optional default value to return if the value-store is empty.
     * @returns The value contained in the store, or the default value if provided. 
     */
    extract_value(def?: T): T,
};

/**
 * Basic implementation using a private optional field as the value-store.
 * 
 * If the constructor is called with a parameter that happens to be `undefined`,
 * the store is treated as being empty rather than a singleton of `undefined.
 * 
 * This behavior is different for `null`, which is always a singleton.
 */
export class OldMaybe<T> implements Optional<T> {
    constructor(private readonly value?: T) { }

    public is_absent(): boolean {
        return (this.value === undefined);
    }

    public is_present(): boolean {
        return !(this.is_absent());
    }

    public extract_value(def?: T): T {
        if (this.is_present()) {
            return this.value;
        } else {
            if (def !== undefined) {
                return def;
            } else {
                throw new Error("OldMaybe<T> has no value to extract and no default was provided");
            }
        }
    }

}

/**
 * Advanced implementation with two static factories instead of a public constructor,
 * which treats any value passed into the [[Some]] factory as being a singleton,
 * and only considers the store empty when created with the [[None]] factory.
 */
export class Maybe<T> implements Optional<T> {
    /**
     * Static factory that always produces a singleton value, even for `null` and `undefined`
     * 
     * @param value Value to store
     * @returns Value-store containing singleton of `value`
     */
    public static Some<T>(value: T): Maybe<T> {
        return new this(true, value);
    }

    /**
     * Static factory that always produces an empty value.
     * @returns An empty value-store 
     */
    public static None<T>(): Maybe<T> {
        return new this(false);
    }

    private constructor(private readonly _has_value: boolean, private readonly value?: T) { }

    public is_absent(): boolean {
        return !(this._has_value);
    }

    public is_present(): boolean {
        return this._has_value;
    }

    public extract_value(def?: T): T {
        if (this._has_value) {
            return this.value;
        } else {
            if (def !== undefined) {
                return def;
            } else {
                throw new Error("Maybe<T> has no value to extract and no default was provided");
            }
        }
    }
}

/**
 * Variant of [[Maybe]] that uses an `Array<T>` as an empty-or-singleton value-store
 */
export class ListMaybe<T> implements Optional<T> {
    /**
     * Static factory that always produces a singleton value, even for `null` and `undefined`
     * 
     * @param value Value to store
     * @returns Value-store containing singleton of `value`
     */
    public static Some<T>(value: T): ListMaybe<T> {
        return new this([value]);
    }

    /**
     * Static factory that always produces an empty value.
     * @returns An empty value-store 
     */
    public static None<T>(): ListMaybe<T> {
        return new this([]);
    }

    private constructor(private readonly value_store: T[]) { }

    public is_absent(): boolean {
        return (this.value_store.length === 0);
    }

    public is_present(): boolean {
        return (this.value_store.length !== 0);
    }

    public extract_value(def?: T): T {
        if (this.value_store.length !== 0) {
            return this.value_store[0];
        } else {
            if (def !== undefined) {
                return def;
            } else {
                throw new Error("ListMaybe<T> has no value to extract and no default was provided");
            }
        }
    }
}
export interface LIFO<T> {
    push(val: T): void;
    pop(): T | undefined;
    peek(): T | undefined;
    peek_def(def: T): T;
    size(): number;
}
export class Stack<T> implements LIFO<T> {
    /** Constructor pops `_stack` argument elements in retrograde */
    constructor(protected _stack: T[] = new Array<T>()) { }

    /**
     * push: Adds a value to the base (head) of the stack, superceding the old head
     */
    public push(val: T) {
        this._stack.push(val);
    }

    /**
     * pop: Removes a value from the base (head) of the stack, returning it if present
     * @returns Most recently added element to the stack, or undefined if empty 
     */
    public pop(): T | undefined {
        return this._stack.pop()
    }

    /**
     * peek: Retrieves (without removing) a value from the base (head) of the stack, returning it if present
     * @returns Most recent added element to the stack, or undefined if empty
     */
    public peek(): T | undefined {
        if (this._stack.length == 0) {
            return undefined;
        } else {
            return this._stack[this._stack.length - 1];
        }
    }

    public peek_def(def: T): T {
        if (this._stack.length == 0) {
            return def;
        } else {
            return this._stack[this._stack.length - 1];
        }
    }

    public size(): number {
        return this._stack.length;
    }
}