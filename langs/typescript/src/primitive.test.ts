import {
    bytes,
    bytesencoder, bytesdecoder,
    sequenceencoder, sequencedecoder,
    optiondecoder, optionencoder,
    lenpref_encoder, lenpref_decoder,
    stringdecoder, stringencoder,
    booleanencoder, booleandecoder, paddingencoder, paddingdecoder, reservesuffix_encoder, reservesuffix_decoder
} from './primitive';
import * as fc from 'fast-check';
import * as int from './integral';
import { Encoder } from './encoder'
import { Decoder } from './decoder'
import { Optional, Maybe } from './util';

// ANCHOR - boolean unit tests
describe('booleandecoder+booleanencoder', () => {
    test('boolean_encoder', () => {
        expect(booleanencoder(true)).toBe("ff");
        expect(booleanencoder(false)).toBe("00");
    });

    test('boolean_decoder valid', () => {
        expect(booleandecoder("ff")).toBe(true);
        expect(booleandecoder("00")).toBe(false);
    });

    test('boolean_decoder invalid', () => {
        expect(() => booleandecoder("01")).toThrow();
        expect(() => booleandecoder("fe")).toThrow();
    });
})

// SECTION - bytes unit tests
describe('bytes_encoder+bytes_decoder', () => {
    let nonce_hash = "deadbeef".repeat(8);
    let _gen_bytes = () => {
        let cyc = [0xde, 0xad, 0xbe, 0xef];
        let arr: bytes = new Uint8Array(32);
        for (var i = 0; i < 32; i++) {
            arr[i] = cyc[i % 4];
        }
        return arr;
    }
    let nonce_bytes = _gen_bytes();

    test('encode@bytes', () => {
        expect(bytesencoder()(nonce_bytes)).toBe(nonce_hash);
    });

    test('decode@bytes', () => {
        expect(bytesdecoder()(nonce_hash)).toStrictEqual(nonce_bytes);
    });

    test('fixed-length encoding enforced', () => {
        expect(bytesencoder(32)(nonce_bytes)).toBe(nonce_hash);
        expect(bytesdecoder(32)(nonce_hash)).toStrictEqual(nonce_bytes);
        expect(() => bytesencoder(-1)(nonce_bytes)).toThrow();
        expect(() => bytesencoder(31)(nonce_bytes)).toThrow();
        expect(() => bytesencoder(33)(nonce_bytes)).toThrow();
        expect(() => bytesdecoder(-1)(nonce_hash)).toThrow();
        expect(() => bytesdecoder(33)(nonce_hash)).toThrow();
    });

    test('roundtrip@bytes (encoder is inverse of decoder)', () => {
        fc.assert(fc.property(
            fc.hexaString({ minLength: 32, maxLength: 32 }),
            hex => {
                bytesencoder()(bytesdecoder()(hex)) === hex;
            }
        ));
        fc.assert(fc.property(
            fc.uint8Array({ minLength: 32, maxLength: 32 }),
            bytes => {
                bytesdecoder()(bytesencoder()(bytes)) === bytes;
            }
        ));
    });
});
// !SECTION

describe('stringencoder (raw)', () => {
    test('enforces length parity', () => {
        expect(() => stringencoder("deadbee")).toThrow();
    });

    test('enforces alphabet', () => {
        expect(() => stringencoder("deadbeet")).toThrow();
    });

    test('identity otherwise', () => {
        fc.assert(fc.property(fc.hexaString({ minLength: 0, maxLength: 100 }).filter(s => (s.length % 2) === 0),
            val => (stringencoder(val) === val && stringdecoder(val) === val)
        ));
    });
})

// SECTION - varlen_string unit tests
describe('lenpref_encoder@stringencoder+lenpref_decoder@stringdecoder', () => {
    test('encode@uint30|string', () => {
        expect(lenpref_encoder()(stringencoder)("")).toBe("00000000");
        expect(lenpref_encoder()(stringencoder)("aa")).toBe("00000001aa");
        expect(lenpref_encoder()(stringencoder)("aabb")).toBe("00000002aabb");
        expect(lenpref_encoder()(stringencoder)("123456")).toBe("00000003123456");
        expect(lenpref_encoder()(stringencoder)("deadbeef")).toBe("00000004deadbeef");
    });

    test('encode@uint8|string', () => {
        expect(lenpref_encoder("uint8")(stringencoder)("")).toBe("00");
        expect(lenpref_encoder("uint8")(stringencoder)("aa")).toBe("01aa");
        expect(lenpref_encoder("uint8")(stringencoder)("aabb")).toBe("02aabb");
        expect(lenpref_encoder("uint8")(stringencoder)("123456")).toBe("03123456");
        expect(lenpref_encoder("uint8")(stringencoder)("deadbeef")).toBe("04deadbeef");
    });

    test('decode@uint30|string', () => {
        expect(lenpref_decoder<string, "uint30">()(stringdecoder)("00000000")).toBe("");
        expect(lenpref_decoder<string, "uint30">()(stringdecoder)("00000001aa")).toBe("aa");
        expect(lenpref_decoder<string, "uint30">()(stringdecoder)("00000002aabb")).toBe("aabb");
        expect(lenpref_decoder<string, "uint30">()(stringdecoder)("00000003123456")).toBe("123456");
        expect(lenpref_decoder<string, "uint30">()(stringdecoder)("00000004deadbeef")).toBe("deadbeef");
    });

    test('decode@uint8|string', () => {
        expect(lenpref_decoder<string, "uint8">("uint8")(stringdecoder)("00")).toBe("");
        expect(lenpref_decoder<string, "uint8">("uint8")(stringdecoder)("01aa")).toBe("aa");
        expect(lenpref_decoder<string, "uint8">("uint8")(stringdecoder)("02aabb")).toBe("aabb");
        expect(lenpref_decoder<string, "uint8">("uint8")(stringdecoder)("03123456")).toBe("123456");
        expect(lenpref_decoder<string, "uint8">("uint8")(stringdecoder)("04deadbeef")).toBe("deadbeef");
    });

    test('roundtrip@uint30|string (encoder is inverse of decoder)', () => {
        let roundtrip = (s: string) => { expect(lenpref_encoder()(stringencoder)(lenpref_decoder()(stringdecoder)(s))).toBe(s); };
        roundtrip("00000000");
        roundtrip("000000010f");
        roundtrip("00000002abcd");
        roundtrip("0000000400011011");
        roundtrip("00000006abcdefabcdef");
    });

    test('roundtrip@uint8|string (encoder is inverse of decoder)', () => {
        let roundtrip = (s: string) => { expect(lenpref_encoder("uint8")(stringencoder)(lenpref_decoder("uint8")(stringdecoder)(s))).toBe(s); };
        roundtrip("00");
        roundtrip("010f");
        roundtrip("02abcd");
        roundtrip("0400011011");
        roundtrip("06abcdefabcdef");
    });

    test('roundtrip@uint30|string (decoder is inverse of encoder)', () => {
        let roundtrip = (s: string) => { expect(lenpref_decoder()(stringdecoder)(lenpref_encoder()(stringencoder)(s))).toBe(s); };

        fc.assert(fc.property(
            fc.hexaString({ minLength: 0, maxLength: 100 }).filter(str => (str.length % 2) === 0),
            str => roundtrip(str)
        ));
    });

    test('roundtrip@uint8|string (decoder is inverse of encoder)', () => {
        let roundtrip = (s: string) => { expect(lenpref_decoder("uint8")(stringdecoder)(lenpref_encoder("uint8")(stringencoder)(s))).toBe(s); };

        fc.assert(fc.property(
            fc.hexaString({ minLength: 0, maxLength: 100 }).filter(str => (str.length % 2) === 0),
            str => roundtrip(str)
        ));
    });

});
// !SECTION

// SECTION - option unit tests
describe('option unit tests', () => {
    test('absent value encoding', () => {
        const encoders: Array<Encoder<unknown>> = [
            int.uint8encoder,
            int.int8encoder,
            int.uint16encoder,
            int.int16encoder,
            int.uint32encoder,
            int.int32encoder,
            int.uint64encoder,
            int.int64encoder,
            bytesencoder(),
            lenpref_encoder()(stringencoder),
        ];
        encoders.forEach(enc => {
            expect(optionencoder(enc)(Maybe.None())).toBe("00");
        });
    });

    test('absent value decoding', () => {
        const encoders: Array<Decoder<unknown>> = [
            int.uint8decoder,
            int.int8decoder,
            int.uint16decoder,
            int.int16decoder,
            int.uint32decoder,
            int.int32decoder,
            int.uint64decoder,
            int.int64decoder,
            bytesdecoder(),
            lenpref_decoder()(stringdecoder),
        ];
        encoders.forEach(dec => {
            expect(optiondecoder(dec)("00").is_absent()).toBe(true);
        });
    });

    test('nested option encoding as unary', () => {
        const test_nest = (i: number) => {
            var nest: Optional<unknown> = Maybe.None();
            var dec: Decoder<Optional<unknown>> = optiondecoder(null);
            for (var j = 0; j < i; j++) {
                nest = Maybe.Some(nest);
                dec = optiondecoder(dec);
            }
            expect(dec("ff".repeat(i) + "00")).toStrictEqual(nest);
        };
        for (var i = 0; i < 100; i++) {
            test_nest(i);
        }
    });

    test('nested option decoding as unary', () => {
        const test_nest = (i: number) => {
            var nest: Optional<unknown> = Maybe.None();
            var enc: Encoder<Optional<unknown>> = optionencoder(null);
            for (var j = 0; j < i; j++) {
                nest = Maybe.Some(nest);
                enc = optionencoder(enc);
            }
            expect(enc(nest)).toBe("ff".repeat(i) + "00");
        };
        for (var i = 0; i < 100; i++) {
            test_nest(i);
        }
    });
    // FIXME[epic=test] - add more tests
});
// !SECTION

// SECTION - sequence unit tests
describe('sequence unit tests', () => {
    test('empty sequence encoding', () => {
        const encoders: Array<Encoder<unknown>> = [
            int.uint8encoder,
            int.int8encoder,
            int.uint16encoder,
            int.int16encoder,
            int.uint32encoder,
            int.int32encoder,
            int.uint64encoder,
            int.int64encoder,
            bytesencoder(),
            lenpref_encoder()(stringencoder),
        ];
        encoders.forEach(enc => {
            expect(sequenceencoder(enc)(new Array())).toBe("00000000");
        });
    });

    test('empty sequence decoding', () => {
        const encoders: Array<Decoder<unknown>> = [
            int.uint8decoder,
            int.int8decoder,
            int.uint16decoder,
            int.int16decoder,
            int.uint32decoder,
            int.int32decoder,
            int.uint64decoder,
            int.int64decoder,
            bytesdecoder(),
            lenpref_decoder()(stringdecoder),
        ];
        encoders.forEach(dec => {
            expect(sequencedecoder(dec)("00000000").length).toBe(0);
        });
    });

    test('roundtrip sequence@uint30', () => {
        fc.assert(fc.property(fc.array(fc.boolean()),
            (bs) => {
                const hex = sequenceencoder(booleanencoder, "uint30")(bs);
                const rt = sequencedecoder(booleandecoder, "uint30")(hex);
                return (rt.length === bs.length && rt.every((value, index) => (bs[index] === value)));
            }
        ));
        fc.assert(fc.property(fc.array(fc.integer({ min: 0, max: 0xff })),
            (i) => {
                const hex = sequenceencoder(int.uint8encoder, "uint30")(i);
                const rt = sequencedecoder(int.uint8decoder, "uint30")(hex);
                return (rt.length === i.length && rt.every((value, index) => (i[index] === value)));
            }
        ));
        fc.assert(fc.property(fc.array(fc.hexaString({ minLength: 0, maxLength: 100 }).filter(t => (t.length % 2) === 0)),
            (s) => {
                const hex = sequenceencoder(lenpref_encoder("uint8")(stringencoder), "uint30")(s);
                const rt = sequencedecoder(lenpref_decoder("uint8")(stringdecoder), "uint30")(hex);
                return (rt.length === s.length && rt.every((value, index) => (s[index] === value)));
            }
        ));
    });

    test('roundtrip sequence@uint8', () => {
        fc.assert(fc.property(fc.array(fc.boolean(), { maxLength: 0xff }),
            (bs) => {
                const hex = sequenceencoder(booleanencoder, "uint8")(bs);
                const rt = sequencedecoder(booleandecoder, "uint8")(hex);
                return (rt.length === bs.length && rt.every((value, index) => (bs[index] === value)));
            }
        ));
        fc.assert(fc.property(fc.array(fc.integer({ min: 0, max: 0xff }), { maxLength: 0xff }),
            (i) => {
                const hex = sequenceencoder(int.uint8encoder, "uint8")(i);
                const rt = sequencedecoder(int.uint8decoder, "uint8")(hex);
                return (rt.length === i.length && rt.every((value, index) => (i[index] === value)));
            }
        ));
        fc.assert(fc.property(
            fc.array(fc.hexaString({ minLength: 0, maxLength: 31 }).filter(t => (t.length % 2) === 0), { maxLength: 32 })
                .filter(arr => arr.reduce<number>((prev, current) => prev + (1 + current.length), 0) <= 0xff),
            (s) => {
                const hex = sequenceencoder(lenpref_encoder("uint8")(stringencoder), "uint8")(s);
                const rt = sequencedecoder(lenpref_decoder("uint8")(stringdecoder), "uint8")(hex);
                return (rt.length === s.length && rt.every((value, index) => (s[index] === value)));
            }
        ));
    });

    test('roundtrip sequence@reserve=4', () => {
        const reserve = 4;

        fc.assert(fc.property(fc.array(fc.boolean()),
            (bs) => {
                const hex = sequenceencoder(booleanencoder, reserve)(bs) + paddingencoder(reserve)();
                const rt = sequencedecoder(booleandecoder, reserve)(hex);
                return (rt.length === bs.length && rt.every((value, index) => (bs[index] === value)));
            }
        ));
        fc.assert(fc.property(fc.array(fc.integer({ min: 0, max: 0xff })),
            (i) => {
                const hex = sequenceencoder(int.uint8encoder, reserve)(i) + paddingencoder(reserve)();
                const rt = sequencedecoder(int.uint8decoder, reserve)(hex);
                return (rt.length === i.length && rt.every((value, index) => (i[index] === value)));
            }
        ));
        fc.assert(fc.property(
            fc.array(fc.hexaString({ minLength: 0, maxLength: 100 }).filter(t => (t.length % 2) === 0)),
            (s) => {
                const hex = sequenceencoder(lenpref_encoder("uint8")(stringencoder), reserve)(s) + paddingencoder(reserve)();
                const rt = sequencedecoder(lenpref_decoder("uint8")(stringdecoder), reserve)(hex);
                return (rt.length === s.length && rt.every((value, index) => (s[index] === value)));
            }
        ));
    });
});
// !SECTION

describe('padding', () => {
    test('padding encoder', () => {
        expect(paddingencoder(0)()).toBe("");
        expect(paddingencoder(3)()).toBe("000000");
        expect(() => paddingencoder(-1)()).toThrow();
    });

    test('padding decoder', () => {
        expect(() => paddingdecoder(0)("")).not.toThrow();
        expect(() => paddingdecoder(1)("")).toThrow();
        expect(() => paddingdecoder(1)("00")).not.toThrow();
        expect(() => paddingdecoder(2)("00")).toThrow();
    });
});


describe('ReserveSuffix', () => {
    test('reservesuffix_encoder', () => {
        expect(reservesuffix_encoder(0)(stringencoder)("deadbeef")).toBe("deadbeef");
        expect(reservesuffix_encoder(255)(stringencoder)("deadbeef")).toBe("deadbeef");
    });

    test('reservesuffix_decoder', () => {
        expect(reservesuffix_decoder<string, number>(0)(stringdecoder)("deadbeef")).toBe("deadbeef");
        expect(reservesuffix_decoder<string, number>(2)(stringdecoder)("deadbeef")).toBe("dead");
        expect(reservesuffix_decoder<string, number>(4)(stringdecoder)("deadbeef")).toBe("");
        expect(() => reservesuffix_decoder<string, number>(6)(stringdecoder)("deadbeef")).toThrow();
    });
});