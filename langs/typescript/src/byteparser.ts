import { Stack } from './util';

/* Modified clone of @taquito-local-forging/uint8array-consumer */

/**
 * Basic class for pre-parsing hex-strings into byte-arrays and consuming slices of bytes in sequence.
 */
export class ByteParser {
    /**
     * Static creation method that pre-converts a valid hex-string into a new ByteParser object, or
     * throws an error if the input string does not conform to the expected alphabet or length-parity
     * of a hexadecimal-encoded byte-sequence.
     * @param hex Input hex-string to use for creating byte-array buffer
     * @returns ByteParser object initialized to the converted byte-array of hex
     */
    static parse(hex: string) {
        const canonical = hex.toLowerCase();
        /* if we know that length is even and all charcacters are legal hex digits
         * and the string length is even in parity (i.e. it contains an integral number of 2-word
         * hexadecimal byte-representations, parse each sequential aligned 2-word slice into an
         * internal buffer and create a new ByteParser on that buffer.
         *
         * If this condition is not met, either because an illegal character was found or because
         * the length was odd in parity, we throw an error instead.
         */
        if (/^(([a-f]|\d){2})*$/.test(canonical)) {
            const arr = new Uint8Array(
                (canonical.match(/([a-z]|\d){2}/g) || []).map(byte => parseInt(byte, 16))
            );
            return new this(arr);
        } else {
            throw new Error('Invalid hex string');
        }
    }

    /** Offset into the buffer that we have consumed so far.
     * Invariant: offset >= 0
     * Invariant: offset < len
     *
     * All method calls to this class or any subclass should
     * only ever preserve the current value of offset or cause
     * it to increase. It should never decrease.
     * */
    protected offset: number;

    /**
     * Buffer (array of bytes) being parsed
     */
    protected readonly arr: Uint8Array;

    /**
     * Memoized value of arr.length, to avoid unnecessary indirection
     * (this might be overkill)
     */
    protected readonly len: number;

    /**
     * A mutable stack whose elements are hierarchically nested 'goals' that the
     * offset is required to reach after an unspecified sequence of offset-advancing
     * method calls, to which values are prepended when new goals are established
     * and to which values are removed as existing goals are reached.
     * 
     * Because each layered goal defines a hard limit on how far the offset is allowed
     * to advance in the evaluation of any method, each fresh goal we attempt to add
     * to the front of the stack must be less than or equal in value to the most recently
     * added goal at the time of prepending (otherwise we can exceed a goal by pushing a less
     * restrictive limit).
     * 
     * Invariant: (target_offsets.peek() === undefined || (target_offset.peek() >= offset && target_offset.peek() <= len))
     * This invariant is also an invariant of offset.
     */
    protected target_offsets: Stack<number>;

    constructor(array: Uint8Array, off: number = 0) {
        this.arr = array;
        this.offset = off;
        this.len = array.length;
        this.target_offsets = new Stack<number>();
    }

    /**
     * Attempts to extract from the buffer a sub-array of the desired length, starting at the current
     * offset position. If successful, the offset is advanced by the appropriate amount (equal to the length consumed)
     * and all fulfilled target offsets are removed from their stack.
     *
     * Fails under the following conditions:
     * - `nbytes` is negative
     * - there are fewer than `nbytes` bytes remaining in the buffer,
     * - [[target_offsets]] is non-empty and the most recently added element would be exceeded
     *
     * @param nbytes number of bytes we wish to consume
     * @returns a [[Uint8Array]] of length `nbytes` (if no error is thrown)
     */
    public consume(nbytes: number): Uint8Array {
        /* original version did not check for non-positive lengths or out-of-bounds */
        if (nbytes < 0) {
            throw new Error('Cannot consume negative bytes');
        } else if (this.offset + nbytes > this.len) {
            throw new Error('Cannot consume past end of buffer');
        }
        var current_goal = this.target_offsets.peek();
        if (current_goal !== undefined && this.offset + nbytes > current_goal) {
            throw new Error('Cannot consume beyond the most recently set target offset');
        }

        const ret = this.arr.subarray(this.offset, this.offset + nbytes);
        this.offset += nbytes;

        return ret;
    }


    /**
     * Attempts to extract from the buffer a single byte at index [[ix]], counting the first unconsumed byte as index 0 
     * If successful, the value at that index is returned, without changing the value of the internal offset.
     *
     * Fails under the following conditions:
     * - `ix` is negative
     * - there are fewer than `ix` bytes remaining in the buffer,
     * - [[target_offsets]] is non-empty and the most recently added limit would be exceeded
     *
     * @param ix position (relative to internal offset) of the byte we wish to view
     * @returns value of the byte at the specified position
     */
    protected seek_to(ix: number): number {
        if (ix < 0) {
            throw new Error('Cannot seek backwards');
        } else if (this.offset + ix >= this.len) {
            throw new Error('Cannot seek past end of buffer');
        }

        var current_goal = this.target_offsets.peek();
        if (current_goal !== undefined && this.offset + ix >= current_goal) {
            throw new Error('Cannot seek beyond the most recently set target offset');
        }

        const ret = this.arr[this.offset + ix];
        return ret;
    }

    /**
     * Tests whether the current offset has reached the most recently set target offset
     * 
     * Throws an error if no target offsets are established, or if the offset managed to exceed the goal
     * @returns `true` when offset has reached its target, `false` otherwise
     */
    public test_goal(): boolean {
        var current_goal = this.target_offsets.peek();

        if (current_goal === undefined) {
            throw new Error("No target offset to check against, there is probably an unmatched call to test_goal somewhere")
        } else if (this.offset > current_goal) {
            throw new Error("Invariant failed: offset managed to exceed its target");
        }
        return (this.offset == current_goal);
    }

    /**
     * Enforces an assertion that the current value of offset has reached its most recently
     * established target, and removes that target if this is the case.
     * 
     * Should only ever be called after a call to [[test_goal]] returns `true`.
     */
    public enforce_goal(): void {
        var current_goal = this.target_offsets.peek();

        if (current_goal === undefined) {
            throw new Error("No target offset to enforce; there is probably an unmatched call to enforce_goal somewhere");
        } else if (this.offset < current_goal) {
            throw new Error("Assertion failed: offset failed to reach most recent target at calltime of enforce_goal");
        } else if (this.offset > current_goal) {
            throw new Error("Invariant failed: offset managed to exceed its target");
        } else {
            this.target_offsets.pop();
        }
    }


    /**
     * Creates a new context-window of byte-width `width` by adding a new target offset
     * equal to the sum of the current offset and `width`.
     * 
     * Fails if the current context-window is narrower than the new context-window that
     * we would be wrapping it around, or if there are fewer than `width` bytes remaining
     * in the current 
     *  
     * @param width Byte-width of the newly-established context window
     * @returns 
     */
    public set_fit(width: number): void {
        if (width < 0) {
            throw new Error('Fit-width cannot be set to negative value');
        } else if (width == 0) {
            this.target_offsets.push(this.offset);
            return;
        }

        let limit = this.target_offsets.peek_def(this.len);

        if (this.offset + width <= limit) {
            this.target_offsets.push(this.offset + width);
            return;
        }

        if (this.target_offsets.size() == 0) {
            throw new Error('Fit-width to be set would set a target outside of buffer bounds')
        } else {
            throw new Error('Fit-width to be set would set a target outside of current context-window');
        }
    }

    /**
     * Creates a new context-window of byte-width `n - delta` where `n` is either the byte-width of the
     * current context-window or `len`.
     * 
     * Fails if `n` is less than `delta`
     *  
     * @param delta value to decrement from current context-window width
     */
    public tighten_fit(delta: number): void {
        let limit = this.target_offsets.peek_def(this.len);

        if (this.offset <= limit - delta) {
            this.target_offsets.push(limit - delta);
        } else {
            throw new Error('Target offset delta cannot be greater than current window width');
        }
    }

    /**
     * Accesses the maximum length we are allowed to read in the current evalutation context.
     *
     * If the target_offsets stack is non-empty, we measure the width of the current evaluation window,
     * and otherwise measure the distance from the end of the buffer.
     *
     * @returns The maximum number of bytes that can be safely consumed without exceeding the current goal (if any) or overruning the buffer
     */
    public length() {
        const limit = this.target_offsets.peek();
        return ((limit === undefined) ? this.len : limit) - this.offset;
    }
}

export class TypedParser extends ByteParser {
    /**
     * Returns a TypedParser object from an input value, which can either be a
     * hex-string (in which case a fresh TypedParser is constructed) or a
     * TypedParser (in which case the argument is simply returned unmodified)
     * @param hex Either a hex-string to begin parsing, or an already-constructed TypedParser
     */
    static parse(hex: string | TypedParser) {
        if (typeof hex === "string") {
            var model: TypedParser = super.parse(hex) as TypedParser;
            return new this(model.arr);
        } else {
            return hex;
        }
    }

    /**
     * Consumes one byte of input and converts to an unsigned integer.
     *
     * @returns 8-bit unsigned integral value corresponding to the byte consumed.
     */
    public get_uint8(): number {
        var array = this.consume(1);
        var view = new DataView(array.buffer);
        return view.getUint8(array.byteOffset);
    }

    /**
     * Consumes one byte of input and converts to a signed integer.
     *
     * @returns 8-bit signed integral value corresponding to the byte consumed.
     */
    public get_int8(): number {
        var array = this.consume(1);
        var view = new DataView(array.buffer);
        return view.getInt8(array.byteOffset);
    }

    /**
     * Consumes 2 bytes of input and converts to an unsigned integer.
     *
     * @returns 16-bit unsigned integral value corresponding to the 2-byte sequence consumed.
     */
    public get_uint16(): number {
        var array = this.consume(2);
        var view = new DataView(array.buffer);
        return view.getUint16(array.byteOffset);
    }

    /**
     * Consumes 2 bytes of input and converts to a signed integer.
     *
     * @returns 16-bit signed integral value corresponding to the 2-byte sequence consumed.
     */
    public get_int16(): number {
        var array = this.consume(2);
        var view = new DataView(array.buffer);
        return view.getInt16(array.byteOffset);
    }

    /**
     * Consumes 4 bytes of input and converts to an unsigned integer.
     * 
     * @returns 32-bit unsigned integral value corresponding to the 4-byte sequence consumed.
     */
    public get_uint32(): number {
        var array = this.consume(4);
        var view = new DataView(array.buffer);
        return view.getUint32(array.byteOffset);
    }

    /**
     * Consumes 4 bytes of input and converts to a signed integer.
     *
     * @returns 32-bit signed integral value corresponding to the 4-byte sequence consumed.
     */
    public get_int32(): number {
        var array = this.consume(4);
        var view = new DataView(array.buffer);
        return view.getInt32(array.byteOffset);
    }

    /**
     * Consumes 8 bytes of input and converts to an unsigned integer.
     * 
     * @returns 64-bit unsigned integral value corresponding to the 8-byte sequence consumed.
     */
    public get_uint64(): bigint {
        var array = this.consume(8);
        var view = new DataView(array.buffer);
        return view.getBigUint64(array.byteOffset);
    }

    /**
     * Consumes 8 bytes of input and converts to a signed integer.
     *
     * @returns 64-bit signed integral value corresponding to the 8-byte sequence consumed.
     */
    public get_int64(): bigint {
        var array = this.consume(8);
        var view = new DataView(array.buffer);
        return view.getBigInt64(array.byteOffset);
    }

    /**
     * Consumes 4 bytes of input and converts to a single-precision floating point number.
     *
     * @returns Single-precision floating point value corresponding to the 4-byte sequence consumed.
     */
    public get_float32(): number {
        var array = this.consume(4);
        var view = new DataView(array.buffer);
        return view.getFloat32(array.byteOffset);
    }

    /**
     * Consumes 8 bytes of input and converts to a double-precision floating point number.
     *
     * @returns Double-precision floating point value corresponding to the 8-byte sequence consumed.
     */
    public get_float64(): number {
        var array = this.consume(8);
        var view = new DataView(array.buffer);
        return view.getFloat64(array.byteOffset);
    }

    public get_boolean(): boolean {
        const testbyte = this.consume(1)[0];
        switch (testbyte) {
            case 0x00:
                return false;
            case 0xff:
                return true;
            default:
                const expr = testbyte.toString(16).padStart(2, "0");
                throw new Error(`Expected boolean value (i.e. either 0xff or 0x00), found ${expr}`)
        }
    }

    /**
     * Consumes a dynamic number of bytes corresponding to however many bytes can
     * be consumed in the current context.
     * 
     * If a value of [[length]] is provided, this is used instead. Note that this can cause an
     * error if the length parameter is larger than the upper limit on the current consumable bytes.
     *
     * @param length Number of bytes in the dynamic string (defaults to maximum consumable bytes).
     * @returns ArrayBuffer containing the raw bytes consumed.
     */
    public get_dynamic_string(length = this.length()): ArrayBuffer {
        var arr = this.consume(length);
        return arr;
    }

    public skip_padding(length: number): void {
        this.consume(length);
    }

    /**
     * Consumes and returns an arbitrary-length sequence of bytes up until (and including) the first
     * byte for which a terminal predicate holds. Used primarily for Zarith integers and naturals.
     * 
     * @param is_final Predicate function that returns true only for the final byte in the sequence
     * @returns Fresh Uint8Array containing the arbitrary-length self-terminating byte-sequence
     */
    public get_self_terminating(is_final: (byte: number) => boolean): Uint8Array {
        var nbytes = 0;
        while (true) {
            if (is_final(this.seek_to(nbytes++))) {
                break;
            }
        }

        return new Uint8Array(this.consume(nbytes));
    }
}