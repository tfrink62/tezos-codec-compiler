/* buf2hex taken from taquito-utils.ts */
import { ByteParser, TypedParser } from './byteparser'
import { decoder_argument, Decoder } from './decoder';
import { Encoder } from './encoder';
import { Maybe, Optional } from './util'

export const buf2hex = (buffer: ArrayBuffer): string => {
    const byteArray = new Uint8Array(buffer);
    return arr2hex(byteArray);
};

export const arr2hex = (byteArray: Uint8Array): string => {
    const hexParts: string[] = [];
    byteArray.forEach((byte: any) => {
        const hex = byte.toString(16);
        const paddedHex = `00${hex}`.slice(-2);
        hexParts.push(paddedHex);
    });
    return hexParts.join('');
};

export type bytes = Uint8Array;
export type width = "uint8" | "uint30";
export type LenPref<T, U extends width> = T;
export type ReserveSuffix<T, U extends number> = T;
export type varlen_string = LenPref<string, width>;

export const optionencoder = <T>(enc: Encoder<T>): Encoder<Optional<T>> => (val: Optional<T>) => {
    if (val.is_present()) {
        return ("ff" + enc(val.extract_value()));
    } else {
        return "00";
    }
}

export const optiondecoder = <T>(dec: Decoder<T>): Decoder<Optional<T>> => (input: decoder_argument) => {
    var p = TypedParser.parse(input);
    if (p.get_boolean()) {
        return Maybe.Some(dec(p));
    } else {
        return Maybe.None();
    }
}

/**
 * Serializes a homogenous array of elements using the provided element-encoder, using either
 * an initial length prefix of the specified width type, or accepting a promise that the sequence
 * length can be recovered by the corresponding decoder based on a reserved-length suffix of remaining
 * values to serialize within a known-length dynamic context window.
 *
 * @param enc Encoder for an individual element of the provided array
 * @param size "uint8" or "uint30" for a length-prefix of the appropriate variety, or any number (value does not matter as implemented)
 * @returns Encoder for any array whose element type matches the generic type of the provided element-encoder
 */
export const sequenceencoder =
    <T>(enc: Encoder<T>, size: width | number = "uint30"): Encoder<Array<T>> =>
        (val: Array<T>) => {
            const dyn: boolean = (typeof size !== "number");

            if (dyn) {
                var byte_count = 0;
            }
            var bytes = "";
            val.forEach(element => {
                const elem: string = enc(element);
                bytes += elem;
                if (dyn) {
                    const bytes_added = (elem.length / 2);
                    byte_count += bytes_added;
                }
            });

            if (dyn) {
                switch (size) {
                    case "uint30":
                        if (byte_count >= (2 ** 30)) {
                            throw new Error("Total number of bytes in sequence must be representable as a 30-bit unsigned integer");
                        }
                        var arr = new ArrayBuffer(4);
                        var view = new DataView(arr);
                        view.setUint32(0, byte_count);
                        return (buf2hex(arr) + bytes);
                    case "uint8":
                        if (byte_count >= (2 ** 8)) {
                            throw new Error("Total number of bytes in sequence must be representable as an 8-bit unsigned integer");
                        }
                        var arr = new ArrayBuffer(1);
                        var view = new DataView(arr);
                        view.setUint8(0, byte_count);
                        return (buf2hex(arr) + bytes);
                }
            }
            return bytes;
        }

/**
 * Parses an encoded sequence into an Array whose element type is determined by the generic type of the element-decoder
 * Handles both explicitly length-prefixed sequences, and sequences whose length can be recovered by narrowing the current
 * context-window enough to leave space for a fixed-length suffix.
 *
 * @param dec Decoder for an individual element of the wire-form array
 * @param size "uint30" or "uint8" as appropriate for a length-prefixed sequence, or the size of the suffix to reserve
 * @returns Decoder for any wire-form Seq-type whose element type matches the generic type of the provided element-decoder
 */
export const sequencedecoder = <T>(dec: Decoder<T>, size: width | number = "uint30"): Decoder<Array<T>> =>
    (input: decoder_argument) => {
        var ret: Array<T> = new Array<T>();
        var p = TypedParser.parse(input);
        if (typeof size !== "number") {
            switch (size) {
                case "uint30":
                    p.set_fit(p.get_uint32());
                    break;
                case "uint8":
                    p.set_fit(p.get_uint8());
                    break;
            }
        } else {
            p.tighten_fit(size);
        }
        while (!p.test_goal()) {
            ret.push(dec(p));
        }
        p.enforce_goal();
        return ret;
    }

/**
 * Encodes a byte-array into an ASCII string where each byte in the array
 * is serialized into a 2-word hexadecimal sub-string.
 * 
 * @param val byte-array to encode as hex-string
 * @param width number of bytes in the array, if it is modelling a fixed-width value
 * @returns hexadecimal string encoding of the array
 */
export const bytesencoder = (width?: number): Encoder<bytes> => (val: bytes) => {
    if (width !== undefined && width != val.length) {
        throw new Error(`bytesencoder: expected fixed-length of ${width}, given byte-array of length ${val.length}.`);
    } else {
        return buf2hex(val);
    }
}

/**
 * Decodes an ASCII string consisting of hexadecimal byte-representations
 * into an array of bytes, where the value at index `i` of the output is the numeric
 * quantity associated with the 2-word hexadecimal substring starting at position `2*i`
 * in the input string.
 *
 * Optionally accepts a byte-width argument to use for when the bytestring models a
 * fixed-size value (such as the output of a hash function, or a CSPRNG seed)
 *
 * @param input the hex-string to decode, or a pre-existing TypedParser to continue using as-is
 * @param width fixed length of the output byte-array, omitted for variable-width byte-arrays
 * @returns array of decoded bytes, up to `width` in length (constrained by length of input buffer)
 */
export const bytesdecoder = (width?: number): Decoder<bytes> => (input: decoder_argument) => {
    var p: TypedParser = TypedParser.parse(input);
    const len: number = (width ?? p.length());
    if (len > p.length()) {
        throw new Error(`Cannot extract ${width}-byte array from buffer of length ${p.length()}.`);
    }
    var arr = new Uint8Array(len);
    for (var i = 0; i < len; i++) arr[i] = p.get_uint8();
    return arr;
}

/**
 * Parses a dynamically-sized hex string whose bounds are implicitly the remainder of the curernt context window
 * (or the entirety of the input hex-string, if passed in directly)
 *
 * @param input string to parse, or a TypedParser object
 * @returns A raw hex-string of the maximal length that can be read in the current context window
 */
export const stringdecoder: Decoder<string> = (input: decoder_argument) => {
    var p = TypedParser.parse(input);
    var arr = p.get_dynamic_string();
    return buf2hex(arr);
}

/**
 * Serializes a raw hex-string via the identity function, after checking that it
 * is valid as a hex-string (length parity is even, hexadecimal characters only).
 *
 * @param val hex-string to be serialized (via validating identity function)
 * @returns val
 */
export const stringencoder: Encoder<string> = (val: string) => {
    if (!/^([A-Fa-f0-9]{2})*$/.test(val)) {
        throw new Error(`Invalid hex string: ${val}`);
    }

    return val;
}

export const lenpref_decoder = <T, U extends width>(width = "uint30" as U) =>
    (dec: Decoder<T>): Decoder<LenPref<T, U>> =>
        (input: decoder_argument) => {
            var p = TypedParser.parse(input);
            var len;
            switch (width) {
                case "uint30":
                    len = p.get_uint32();
                    break;
                case "uint8":
                    len = p.get_uint8();
                    break;
            }
            p.set_fit(len);
            const ret = dec(p);
            p.enforce_goal();
            return ret;
        }

export const lenpref_encoder = <T, U extends width>(width = "uint30" as U) =>
    (enc: Encoder<T>): Encoder<LenPref<T, U>> =>
        (val: T) => {
            var len: number;
            const payload = enc(val);
            if (payload.length % 2 == 0) {
                len = payload.length / 2;
            } else {
                throw new Error("encoding is not a whole number of bytes");
            }

            var arr;
            switch (width) {
                case "uint30":
                    arr = new ArrayBuffer(4);
                    var view = new DataView(arr);
                    view.setUint32(0, len);
                    break;
                case "uint8":
                    arr = new ArrayBuffer(1);
                    var view = new DataView(arr);
                    view.setUint8(0, len);
                    break;
            }
            return (buf2hex(arr) + payload);
        }


export const reservesuffix_decoder = <T, U extends number>(reserve = 0 as U) =>
    (dec: Decoder<T>): Decoder<ReserveSuffix<T, U>> =>
        (input: decoder_argument) => {
            var p = TypedParser.parse(input);
            p.tighten_fit(reserve);
            const ret = dec(p);
            p.enforce_goal();
            return ret;
        }

export const reservesuffix_encoder = <T, U extends number>(reserve = 0 as U) => (enc: Encoder<T>): Encoder<ReserveSuffix<T, U>> => (val: T) => enc(val);


export const booleanencoder: Encoder<boolean> = (val: boolean) => {
    return (val ? "ff" : "00");
}

export const booleandecoder: Decoder<boolean> = (input: decoder_argument) => {
    return TypedParser.parse(input).get_boolean();
}

export const paddingencoder = (n: number): Encoder<void> => () => {
    return ("00".repeat(n));
}

export const paddingdecoder = (n: number): Decoder<void> => (input: decoder_argument) => {
    TypedParser.parse(input).skip_padding(n);
}