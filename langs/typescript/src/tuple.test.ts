import { Decoder } from "./decoder";
import { Encoder } from "./encoder";
import { uint16, int32, uint16encoder, int32encoder, int32decoder, uint16decoder, uint32encoder, uint32, uint32decoder } from "./integral";
import { varlen_string, stringdecoder, stringencoder, lenpref_encoder, lenpref_decoder, booleanencoder, booleandecoder } from "./primitive";
import * as fc from 'fast-check';
import { Tuple, tuple_encoder, tuple_decoder } from './tuple';

describe('tuple', () => {
    test("encode", () => {
        expect(tuple_encoder(uint32encoder, booleanencoder)([0xdeadbeef, true])).toBe("deadbeefff");
    });

    test("decode", () => {
        expect(tuple_decoder(uint32decoder, booleandecoder)("deadbeefff")).toStrictEqual<Tuple<uint32, boolean>>([0xdeadbeef, true]);
    });
});