import { Encoder } from './encoder'
import { Decoder, decoder_argument } from './decoder'
import { TypedParser } from './byteparser';

export type Tuple<Fst, Snd> = [Fst, Snd];

export const tuple_encoder = <Fst, Snd>(fst_enc: Encoder<Fst>, snd_enc: Encoder<Snd>): Encoder<Tuple<Fst, Snd>> => (val: Tuple<Fst, Snd>): string => {
    var fst: string = fst_enc(val[0]);
    var snd: string = snd_enc(val[1]);
    return fst + snd;
}

export const tuple_decoder = <Fst, Snd>(fst_dec: Decoder<Fst>, snd_dec: Decoder<Snd>): Decoder<Tuple<Fst, Snd>> => (input: decoder_argument): Tuple<Fst, Snd> => {
    var p: TypedParser = TypedParser.parse(input);
    const fst: Fst = fst_dec(p);
    const snd: Snd = snd_dec(p);
    return [fst, snd];
}