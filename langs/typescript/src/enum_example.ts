import { Encoder } from './encoder'
import { Decoder } from './decoder'
import { int32decoder, int32encoder } from './integral'

export enum Foo {
    A = 1,
    B = 2,
    C = 4,
    D = 8
};

export const Foo_encoder: Encoder<Foo> = (val) => {
    return int32encoder(val);
}

export const Foo_decoder: Decoder<Foo> = (input) => {
    let ret = int32decoder(input);
    if (typeof Foo[ret] !== 'undefined') {
        return (ret as Foo);
    } else {
        throw new Error("Value cannot be decoded as Foo: out of range");
    }
}