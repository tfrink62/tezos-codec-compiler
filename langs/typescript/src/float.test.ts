import { float32, float32decoder, float32encoder, float64, float64encoder, float64decoder } from './float';
import * as fc from 'fast-check';

/* float32 tests */
describe('unit tests for float32', () => {
    const encoder = float32encoder;
    const decoder = float32decoder;
    const length = 8;
    const hexbounds = { minLength: length, maxLength: length };

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.float({ next: true }),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

/* float32 tests */
describe('unit tests for float64', () => {
    const encoder = float64encoder;
    const decoder = float64decoder;
    const length = 16;
    const hexbounds = { minLength: length, maxLength: length };

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.hexaString(hexbounds),
            str => { encoder(decoder(str)) === str; }
        ));
        fc.assert(fc.property(
            fc.double({ next: true }),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});