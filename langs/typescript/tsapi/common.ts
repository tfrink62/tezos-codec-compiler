import { bytes, bytesdecoder, bytesencoder, LenPref, lenpref_decoder, reservesuffix_decoder, sequenceencoder } from '../src/primitive'
import * as codec from './codec'
import { int8 } from '../src/integral';

const bs58check = require('bs58check');

export const b58_to_bytes = (val: string, prefix_len: number, expected_len?: number): bytes => {
    const ret: bytes = bs58check.decode(val).slice(prefix_len);
    if (expected_len !== undefined && ret.length != expected_len) {
        throw new Error(`b58_to_bytes: expected length ${expected_len}, actual length ${ret.length}`);
    }
    return new Uint8Array(ret);
}

export const string_to_branch = (val: string): bytes => b58_to_bytes(val, 2, 32);

import operation_unsigned = codec.Proto010_PtGRANAD__operation__unsigned;

export const string_to_pkh = (val: string): operation_unsigned.NSpublic_key_hash.public_key_hash => {
    /* NOTE[epic=hardcoded]: Turning two magic numbers into a single constant */
    const pkh_prefix_len = 3;
    const pkh_fixed_len = 20;
    const pkhPrefix = val.substr(0, pkh_prefix_len);
    switch (pkhPrefix) {
        case 'tz1':
            return {
                _tag: operation_unsigned.NSpublic_key_hash.public_key_hash_tag.Ed25519,
                Ed25519__Public_key_hash: b58_to_bytes(val, pkh_prefix_len, pkh_fixed_len),
            };
        case 'tz2':
            return {
                _tag: operation_unsigned.NSpublic_key_hash.public_key_hash_tag.Secp256k1,
                Secp256k1__Public_key_hash: b58_to_bytes(val, pkh_prefix_len, pkh_fixed_len),
            };
        case 'tz3':
            return {
                _tag: operation_unsigned.NSpublic_key_hash.public_key_hash_tag.P256,
                P256__Public_key_hash: b58_to_bytes(val, pkh_prefix_len, pkh_fixed_len),
            };
        default:
            throw new Error('Invalid public key hash');
    }
};

export const string_to_public_key = (val: string): operation_unsigned.NSpublic_key.public_key => {
    const pubkey_prefix_len = 4;
    const pubkey_fixed_len = 32;
    const pubkeyPrefix = val.substr(0, pubkey_prefix_len);
    switch (pubkeyPrefix) {
        case 'edpk':
            return {
                _tag: operation_unsigned.NSpublic_key.public_key_tag.Ed25519,
                Ed25519__Public_key: b58_to_bytes(val, pubkey_prefix_len, pubkey_fixed_len),
            };
        case 'sppk':
            return {
                _tag: operation_unsigned.NSpublic_key.public_key_tag.Secp256k1,
                Secp256k1__Public_key: b58_to_bytes(val, pubkey_prefix_len, pubkey_fixed_len),
            };
        case 'p2pk':
            return {
                _tag: operation_unsigned.NSpublic_key.public_key_tag.P256,
                P256__Public_key: b58_to_bytes(val, pubkey_prefix_len, pubkey_fixed_len),
            };
        default:
            throw new Error('Invalid PK');
    }
};

export const string_to_contract_hash = (val: string): bytes => b58_to_bytes(val, 3, 20);

export const string_to_contract_id = (val: string): operation_unsigned.NScontract_id.contract_id => {
    const addr_prefix_len = 3;
    const pubkeyPrefix = val.substr(0, addr_prefix_len);
    switch (pubkeyPrefix) {
        case 'tz1':
        case 'tz2':
        case 'tz3':
            return {
                _tag: operation_unsigned.NScontract_id.contract_id_tag.Implicit,
                Signature__Public_key_hash: string_to_pkh(val)
            };
        case 'KT1':
            return {
                _tag: operation_unsigned.NScontract_id.contract_id_tag.Originated,
                Contract_hash: string_to_contract_hash(val)
            };
        default:
            throw new Error('Invalid address');
    }
};

export const string_to_proposal = (proposal: string): bytes => {
    return b58_to_bytes(proposal, 2, 32);
}

export const string_to_proposal_array = (proposals: string[]): LenPref<bytes, "uint30"> => {
    let props = proposals.map(string_to_proposal);
    /** FIXME[epic=stopgap] - we have to convert the array to a single blob because
     * the compiler includes stopgap logic that patches types that want to be arrays of
     * indeterminate-length byte-sequences by instead typing them as length-prefixed byte-sequences.
     */
    let hex = sequenceencoder(bytesencoder(), "uint30")(props);
    return lenpref_decoder<bytes, "uint30">("uint30")(bytesdecoder())(hex);
}

export enum BALLOT {
    yay = 0,
    nay = 1,
    pass = 2,
}

export const string_to_ballot = (ballot: string): int8 => {
    if (ballot in BALLOT) {
        return BALLOT[ballot];
    } else {
        throw new Error(`Unexpected ballot ${ballot}: should be 'yay'|'nay'|'pass'`);
    }
}

export const string_to_raw = (str: string): bytes => reservesuffix_decoder<bytes, 0>(0)(bytesdecoder(32))(str);

export const uni_to_hex = (val: string) => Buffer.from(val, 'utf8').toString('hex');
