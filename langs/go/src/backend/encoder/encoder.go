package encoder

type Encoder interface {
	Encode() string
}