package byteparser

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"math"

	"backend/util"
)

type T struct {
	arr []byte;
	offset uint;
	length uint;
	goals *util.Uintstack;
}

type Hexstring string


type Decoder_argument interface {
	Parse() (*T, error)
}

func (p *T) Parse() (*T, error) {
	return p, nil;
}

func (s Hexstring) Parse() (*T, error) {
	arr, err := hex.DecodeString(string(s));

	var offset, length uint;	
	offset = 0;
	goals := util.New();

	if err != nil {
		length = 0;
	} else {
		length = uint(len(arr));
	}
	return &T{ arr, offset, length, goals }, err
}

func (p *T) consume(nbytes uint) ([]byte, error) {
	if p.offset + nbytes > p.length {
		return []byte{}, fmt.Errorf("cannot consume past end of buffer");
	}
	current_goal, err := p.goals.Peek();
	if err == nil && p.offset + nbytes > current_goal {
		return []byte{}, fmt.Errorf("cannot consume beyond the most recently set target offset");
	}

	ret := p.arr[p.offset:p.offset+nbytes];
	p.offset += nbytes;
	return ret, nil
}

func (p *T) Test_goal() (bool, error) {
	current_goal, err := p.goals.Peek();

	if err != nil {
		return false, fmt.Errorf("no target offset to check against")	
	} else if p.offset > current_goal {
		return false, fmt.Errorf("invariant failed: offset managed to exceed its target")
	}
	return (p.offset == current_goal), nil;
}

func (p *T) Enforce_goal() (ret error) {
	current_goal, err := p.goals.Peek();

	switch {
	case err != nil:
		ret = fmt.Errorf("no target offset to enforce");
	case p.offset > current_goal:
		ret = fmt.Errorf("invariant failed: offset managed to exceed its target");
	case p.offset < current_goal:
		ret = fmt.Errorf("assertion failed: offset failed to reach target");
	default:
		p.goals.Pop();
		ret = nil;
	}
	return
}

func (p *T) Set_fit(width uint) error {
	if width == 0 {
		p.goals.Push(p.offset);
		return nil;
	}

	limit := p.goals.Peek_def(p.length);

	if p.offset + width <= limit {
		p.goals.Push(p.offset + width);
		return nil;
	}

	if p.goals.Size() == 0 {
		return fmt.Errorf("cannot set goal past end of buffer")
	} else {
		return fmt.Errorf("cannot set goal outside of current context window")
	}
}

func (p *T) Tighten_fit(delta uint) error {
	limit := p.goals.Peek_def(p.length);

	if p.offset + delta <= limit {
		p.goals.Push(limit - delta);
		return nil;
	} else {
		return fmt.Errorf("target offset delta wider than remaining context window")
	}
}

func (p *T) Length() uint {
	limit := p.goals.Peek_def(p.length);

	return uint(limit - p.offset);
}

func (p *T) Get_uint8() (uint8, error) {
	arr, err := p.consume(1);
	if err != nil {
		return 0, err
	} else {
		return uint8(arr[0]), nil;
	}
}

func (p *T) Get_int8() (int8, error) {
	arr, err := p.consume(1);
	if err != nil {
		return 0, err
	} else {
		return int8(arr[0]), nil;
	}
}

func (p *T) Get_uint16() (uint16, error) {
	arr, err := p.consume(2);
	if err != nil {
		return 0, err
	} else {
		return binary.BigEndian.Uint16(arr), nil;
	}
}

func (p *T) Get_int16() (int16, error) {
	arr, err := p.consume(2);
	if err != nil {
		return 0, err
	} else {
		return int16(binary.BigEndian.Uint16(arr)), nil;
	}
}

func (p *T) Get_uint32() (uint32, error) {
	arr, err := p.consume(4);
	if err != nil {
		return 0, err
	} else {
		return binary.BigEndian.Uint32(arr), nil;
	}
}

func (p *T) Get_int32() (int32, error) {
	arr, err := p.consume(4);
	if err != nil {
		return 0, err
	} else {
		return int32(binary.BigEndian.Uint32(arr)), nil;
	}
}

func (p *T) Get_uint64() (uint64, error) {
	arr, err := p.consume(8);
	if err != nil {
		return 0, err
	} else {
		return binary.BigEndian.Uint64(arr), nil;
	}
}

func (p *T) Get_int64() (int64, error) {
	arr, err := p.consume(8);
	if err != nil {
		return 0, err
	} else {
		return int64(binary.BigEndian.Uint64(arr)), nil;
	}
}

func (p *T) Get_float32() (float32, error) {
	arr, err := p.consume(4);
	if err != nil {
		return 0, err
	} else {
		return math.Float32frombits(binary.BigEndian.Uint32(arr)), nil;
	}
}

func (p *T) Get_float64() (float64, error) {
	arr, err := p.consume(8);
	if err != nil {
		return 0, err
	} else {
		return math.Float64frombits(binary.BigEndian.Uint64(arr)), nil;
	}
}

func (p *T) Get_boolean() (bool, error) {
	arr, err := p.consume(1);
	if err != nil {
		return false, err;
	}
	switch arr[0] {
	case 0x00: return false, nil;
	case 0xff: return true, nil;
	default:
		return false, fmt.Errorf("expected boolean value (i.e. either 0xff or 0x00), found %02x", arr[0]);
	}
}

func (p *T) Get_dynamic_string(args ...uint) ([]byte, error) {
	var length uint;
	switch len(args) {
	case 0:
		length = p.length;
	case 1:
		length = args[0];
	default:
		return []byte{}, fmt.Errorf("too many arguments: expected 0 or 1");
	}
	return p.consume(length);
}

func (p *T) Skip_padding(length uint) error {
	_, err := p.consume(length);	
	return err;
}

func (p *T) seek_to(ix uint) (byte, error) {
	if p.offset + ix >= p.length {
		return 0x00, fmt.Errorf("cannot seek past end of buffer")
	}

	current_goal := p.goals.Peek_def(p.length)

	if p.offset + ix >= current_goal {
		return 0x00, fmt.Errorf("cannot seek beyond the most recently set target offset")
	}

	return p.arr[p.offset + ix], nil
}

func (p *T) Get_self_terminating(is_final func(byte) bool) ([]byte, error) {
	var nbytes uint = 0;

	for {
		next, err := p.seek_to(nbytes);
		if err != nil {
			return []byte{}, err;
		}
		nbytes++;
		if is_final(next) {
			break
		}
	}

	return p.consume(nbytes);
}