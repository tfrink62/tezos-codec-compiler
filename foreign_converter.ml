open Core
open Abstract_type
open Foreign_ast
open Codec_repr

let int_to_fv x = Foreign_val.(Pure (VPrim (PrimNum (Int.to_string x))))

let width_to_fv w = Foreign_val.(Pure (VPrim (PrimWidth w)))

let name_to_var x = Foreign_val.(Pure (Bound (Ident.Binding.Variable_name x)))

let string_to_lit s = Foreign_val.(Pure (VLit (LitString s)))

let reformat raw_name =
  let label_to_namespace_id = function
    | s when String.(prefix s 1 = "0") -> "Proto" ^ s
    | s -> s
  in
  let from_singleton = function
    | [ hd ] -> hd
    | [] -> failwith "Empty list is not a singleton"
    | _ -> failwith "Multi-element list is not a singleton"
  in
  let init_last l =
    let i = List.length l - 1 in
    List.split_n l i
  in
  let undash = String.substr_replace_all ~pattern:"-" ~with_:"_" in
  String.split ~on:'.' raw_name |> List.map ~f:undash |> init_last
  |> fun (x, y) -> List.rev_map ~f:label_to_namespace_id x, from_singleton y

(** Sanitizes a label (field) identifier by replacing all ['.'] characters with
    ["__"] and all ['-'] characters with ['_']. *)
let sanitize_label lab_id =
  String.split ~on:'.' lab_id
  |> List.map ~f:(String.substr_replace_all ~pattern:"-" ~with_:"_")
  |> String.concat ~sep:"__"

(* ANCHOR - convert_abstract_type *)
let rec convert_abstract_type =
  let open Foreign_type in
  function
  | AbstractBool -> Atom (TPrim PrimBool)
  | AbstractInt (s, b) -> Atom (TPrim (PrimInt (s, b)))
  | AbstractIntRanged ((min, max), bytes) ->
      Atom (TPrim (PrimIntRanged ((min, max), bytes)))
  | AbstractString (VariableLength w) ->
      Atom (TComposite (CTDynWidth (w, Atom (TPrim PrimString))))
  | AbstractBytes (VariableLength w) ->
      Atom (TComposite (CTDynWidth (w, Atom (TPrim PrimOpaqueBytes))))
  | AbstractBytes (FixedLength n) -> Atom (TPrim (PrimOpaqueBytesFixedLen n))
  | AbstractSequence (w, elem) -> (
      (* REVIEW[epic=stopgap] - handle sequence of amorphous blobs as a blob
         itself *)
      match elem with
      | AbstractBytes (DynamicLength | IndeterminateLength) -> (
          match w with
          | None ->
              failwith
                "Both sequence and its elements have indeterminate byte-length"
          | Some width ->
              Atom
                (TComposite (CTDynWidth (width, Atom (TPrim PrimOpaqueBytes)))))
      | AbstractBytes (VariableLength (ReserveSuffix _)) -> assert false
      | _ -> Atom (TComposite (CTArray (convert_abstract_type elem))))
  | AbstractOption value ->
      Atom (TComposite (CTOption (convert_abstract_type value)))
  | AbstractNamed (mwidth, name) -> (
      let qualids, base_name = reformat name in
      let qual =
        let f = function
          | x when String.is_suffix x ~suffix:"_tag" -> Ident.Scope.Type_name x
          | x -> Ident.Scope.Inferred_scope x
        in
        List.map ~f qualids
      in
      let base = Atom (TRefer { qual; base_name }) in
      match mwidth with
      | None -> base
      | Some width -> Atom (TComposite (CTDynWidth (width, base))))
  | AbstractDyn (sz, AbstractSequence (None, inner_typ)) ->
      convert_abstract_type (AbstractSequence (Some (DynPrefix sz), inner_typ))
  | AbstractDyn (sz, inner_type) ->
      Atom
        (TComposite
           (CTDynWidth (DynPrefix sz, convert_abstract_type inner_type)))
  | AbstractPadding n -> Atom (TPrim (PrimPadding n))
  (* REVIEW[epic=design] - We are treating AbstractVoid as zero bytes of padding *)
  | AbstractVoid -> Atom (TPrim (PrimPadding 0))
  | AbstractZarith_N -> Atom (TPrim PrimZarith_N)
  | AbstractZarith_Z -> Atom (TPrim PrimZarith_Z)
  | AbstractBytes DynamicLength -> Atom (TPrim PrimOpaqueBytes)
  | AbstractString DynamicLength -> Atom (TPrim PrimString)
  | AbstractEnum (_, name) ->
      let qualids, base_name = reformat name in
      let qual =
        let f x = Ident.Scope.Inferred_scope x in
        List.map ~f qualids
      in
      Atom (TRefer { qual; base_name })
  | AbstractTuple2 (t1, t2) ->
      let t1' = convert_abstract_type t1 and t2' = convert_abstract_type t2 in
      Atom (TComposite (CTTuple2 (t1', t2')))
  | AbstractFloat precision -> Atom (TPrim (PrimFloat precision))
  | AbstractTagVal { base; elem; _ } ->
      let qualids, base_name = reformat base in
      let qual =
        let f x = Ident.Scope.Inferred_scope x in
        List.map ~f qualids
      in
      Atom (TNarrow { qual; base_name; narrow = elem })
  | other ->
      failwith
        ("unhandled type: " ^ Sexp.to_string ([%sexp_of: abstract_type] other))

(* ANCHOR - gen_transcoder_call *)
let rec gen_transcoder_call ~dir ?on ?(apply = true) typ =
  let open Foreign_val in
  let for_type = convert_abstract_type typ
  and final_args =
    if not apply then []
    else
      match dir with
      | Encode ->
          [
            [
              Option.value
                ~default:(Pure (Bound (Ident.Binding.Variable_name "val")))
                on;
            ];
          ]
      | Decode ->
          [
            [
              Option.value
                ~default:(Pure (Bound (Ident.Binding.Variable_name "p")))
                on;
            ];
          ]
  in
  match typ with
  | AbstractVoid ->
      let final_args =
        if not apply then []
        else match dir with Encode -> [ [] ] | Decode -> final_args
      in
      let args = [ int_to_fv 0 ] :: final_args in
      TranscoderCall { dir; for_type; generics = None; args }
  | AbstractPadding n ->
      let final_args =
        if not apply then []
        else match dir with Encode -> [ [] ] | Decode -> final_args
      in
      let args = [ int_to_fv n ] :: final_args in
      TranscoderCall { dir; for_type; generics = None; args }
  | AbstractBool | AbstractInt _ | AbstractFloat _ | AbstractIntRanged _
  | AbstractZarith_N | AbstractZarith_Z
  | AbstractNamed (None, _) ->
      TranscoderCall { dir; for_type; generics = None; args = final_args }
  | AbstractSequence (None, AbstractBytes IndeterminateLength) ->
      failwith "AbstractSequence cannot be transcoded without a width"
  | AbstractSequence (Some w, AbstractBytes IndeterminateLength) ->
      (* REVIEW[epic=needs_refactor] - Pinning behavior of blob-array as single
         blob *)
      gen_transcoder_call ~dir ?on ~apply (AbstractBytes (VariableLength w))
  | AbstractOption inner_typ | AbstractSequence (_, inner_typ) ->
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args =
        match typ with
        | AbstractOption _ -> [ inner_call ] :: final_args
        | AbstractSequence (Some (DynPrefix w), _) ->
            [ inner_call; Pure (VPrim (PrimWidth w)) ] :: final_args
        | AbstractSequence (Some (ReserveSuffix n), _) ->
            [ inner_call; int_to_fv n ] :: final_args
        | AbstractSequence (None, _) ->
            [%sexp_of: abstract_type] typ |> Sexp.to_string |> fun x ->
            failwith
              ("AbstractSequence cannot be transcoded without a width: " ^ x)
        | _ -> assert false
      in
      TranscoderCall { dir; for_type; generics = None; args }
  | AbstractNamed (Some (ReserveSuffix n), inner_typ) ->
      let inner_typ = AbstractNamed (None, inner_typ) in
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args = [ int_to_fv n ] :: [ inner_call ] :: final_args
      and generics =
        Some
          Foreign_type.
            [
              convert_abstract_type inner_typ; Atom (TLiteral (Int.to_string n));
            ]
      in
      TranscoderCall { dir; for_type; generics; args }
  | AbstractNamed (Some (DynPrefix w), inner_typ) ->
      let inner_typ = AbstractNamed (None, inner_typ) in
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args = [ width_to_fv w ] :: [ inner_call ] :: final_args
      and generics =
        Some
          Foreign_type.
            [ convert_abstract_type inner_typ; Atom (TPrim PrimWidth) ]
      in
      TranscoderCall { dir; for_type; generics; args }
  | AbstractDyn (sz, AbstractSequence (None, inner_typ)) ->
      gen_transcoder_call
        ~dir
        ?on
        ~apply
        (AbstractSequence (Some (DynPrefix sz), inner_typ))
  | AbstractDyn (sz, inner_typ) ->
      let inner_call = gen_transcoder_call ~dir ~apply:false inner_typ in
      let args = [ width_to_fv sz ] :: [ inner_call ] :: final_args
      and generics =
        Some
          Foreign_type.
            [ convert_abstract_type inner_typ; Atom (TPrim PrimWidth) ]
      in
      TranscoderCall { dir; for_type; generics; args }
  | AbstractString kind -> (
      match kind with
      | FixedLength n ->
          let args = [ int_to_fv n ] :: final_args in
          TranscoderCall { dir; for_type; generics = None; args }
      | VariableLength (DynPrefix w) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractString DynamicLength)
          in
          let args = [ width_to_fv w ] :: [ inner_call ] :: final_args
          and generics =
            Some
              Foreign_type.[ Atom (TPrim PrimString); Atom (TPrim PrimWidth) ]
          in
          TranscoderCall { dir; for_type; generics; args }
      | VariableLength (ReserveSuffix n) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractString DynamicLength)
          in
          let args = [ int_to_fv n ] :: [ inner_call ] :: final_args
          and generics =
            Some
              Foreign_type.
                [ Atom (TPrim PrimString); Atom (TLiteral (Int.to_string n)) ]
          in
          TranscoderCall { dir; for_type; generics; args }
      | DynamicLength ->
          TranscoderCall { dir; for_type; generics = None; args = final_args }
      | IndeterminateLength ->
          failwith
            "Cannot produce transcoder for indeterminate-length AbstractString")
  | AbstractBytes kind -> (
      match kind with
      | FixedLength n ->
          let args = [ int_to_fv n ] :: final_args in
          TranscoderCall { dir; for_type; generics = None; args }
      | VariableLength (DynPrefix w) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractBytes DynamicLength)
          in
          let args = [ width_to_fv w ] :: [ inner_call ] :: final_args
          and generics =
            Some
              Foreign_type.
                [ Atom (TPrim PrimOpaqueBytes); Atom (TPrim PrimWidth) ]
          in
          TranscoderCall { dir; for_type; generics; args }
      | VariableLength (ReserveSuffix n) ->
          let inner_call =
            gen_transcoder_call ~dir ~apply:false (AbstractBytes DynamicLength)
          in
          let args = [ int_to_fv n ] :: [ inner_call ] :: final_args
          and generics =
            Some
              Foreign_type.
                [
                  Atom (TPrim PrimOpaqueBytes);
                  Atom (TLiteral (Int.to_string n));
                ]
          in
          TranscoderCall { dir; for_type; generics; args }
      | DynamicLength ->
          TranscoderCall
            { dir; for_type; generics = None; args = [] :: final_args }
      | IndeterminateLength ->
          failwith
            "Cannot produce transcoder for indeterminate-length AbstractBytes")
  | AbstractEnum _ ->
      let args = final_args in
      TranscoderCall { dir; for_type; generics = None; args }
  | AbstractTuple2 (t1, t2) ->
      let inner_call1 = gen_transcoder_call ~dir ~apply:false t1
      and inner_call2 = gen_transcoder_call ~dir ~apply:false t2 in
      let generics = Some [ convert_abstract_type t1; convert_abstract_type t2 ]
      and args = [ inner_call1; inner_call2 ] :: final_args in
      TranscoderCall { dir; for_type; generics; args }
  | other ->
      failwith
        ("Unhandled transcoder case " ^ Sexp.to_string
        @@ [%sexp_of: abstract_type] other)

module Gen = struct
  module Context = struct
    module S = Stdlib.Map.Make (Qualification)

    type t = Qualification.t S.t

    let fresh () = S.empty

    let add_assoc k v ctxt = S.add k v ctxt

    (* FIXME[epic=needs_refactor] - work out the proper case-logic *)

    let lookup k ctxt =
      match S.find_opt k ctxt with
      | Some v -> v
      | None -> ( match k with [] -> assert false | _ -> k)

    let reset_local ctxt =
      S.filter
        (fun k _ ->
          match k with
          | [ q ] ->
              not @@ String.is_prefix ~prefix:"X_" @@ Ident.Scope.to_string q
          | _ -> true)
        ctxt
  end

  (* FIXME[epic=needs_refactor] - figure out exactly where to call mk_ident *)

  let mk_ident id = id
  (* let mk_legal = function | '-' -> "_" | ',' -> "_COMMA_" | '+' -> "_PLUS_" |
     '(' | ')' | ' ' -> "" | c -> String.of_char c in id |> String.concat_map
     ~sep:"" ~f:mk_legal *)

  let mk_ns ?prefix id = Qualification.mk_ns ?prefix id

  let transcoder_ident ~dir id =
    let join_id ~affix base = String.concat ~sep:"_" [ mk_ident base; affix ] in
    let affix = match dir with Encode -> "encoder" | Decode -> "decoder" in
    join_id id ~affix

  let enum_ident ~ix id =
    let anon_prefix = "ANON" in
    match id with
    | None -> anon_prefix ^ Int.to_string ix
    | Some name -> mk_ident name

  let tag_ident id =
    (* Printf.eprintf "Tagging ident: %s\n" id; *)
    let affix = "tag" in
    String.concat ~sep:"_" [ id; affix ]

  module Simple = struct
    let cast_record ?ix = function
      | Anon t ->
          let pseudo =
            match ix with
            | None -> "_anon"
            | Some n -> "_anon" ^ Int.to_string n
          in
          Record [ pseudo, t ]
      | Record _ as r -> r
      | _ -> assert false

    let add_tag ~tag_type_name ~tag_size ~tag_name tag =
      let tag_field =
        let name = mk_ident tag_name in
        ( "_tag",
          AbstractTagVal
            { base = tag_type_name; tag_size; elem = name; value = tag } )
      in
      fun x ->
        match cast_record ~ix:tag x with
        | Record rs -> Record (tag_field :: rs)
        | _ -> assert false

    (* REVIEW[epic=design] - AbstractVoid as 0-width padding *)
    let remove_padding =
      List.filter ~f:(function
          | _, (AbstractPadding _ | AbstractVoid) -> false
          | _ -> true)

    module Anon = struct
      let typedec ~qual ~base_name typ =
        let definition = convert_abstract_type typ in
        Binding_mod.exposed_definite, TypeDec { qual; base_name; definition }

      let encoder ~qual ~type_name typ =
        let open Foreign_type in
        let dir = Encode and arg_name = "val" in
        let rhs = [ Return (Some (gen_transcoder_call ~dir typ)) ] in
        let intype = Atom (TRefer { qual; base_name = mk_ident type_name })
        and rhs_sig = Some (Atom (TPrim PrimString))
        and base_name = transcoder_ident ~dir type_name in
        let params = `Typed [ arg_name, Mandatory, intype ] in
        ( Binding_mod.lambda,
          NamedLambda { qual; base_name; params; rhs_sig; rhs } )

      let decoder ~qual ~type_name typ =
        let open Foreign_type in
        let dir = Decode and arg_name = "input" in
        let base_name = transcoder_ident ~dir type_name
        and rhs =
          [
            Return
              (Some (gen_transcoder_call ~dir ~on:(name_to_var arg_name) typ));
          ]
        and rhs_sig =
          Some (Atom (TRefer { qual; base_name = mk_ident type_name }))
        in
        let params =
          let intype = Atom (TPrim PrimDecoderInput) in
          `Typed [ arg_name, Mandatory, intype ]
        in
        ( Binding_mod.lambda,
          NamedLambda { qual; base_name; params; rhs_sig; rhs } )
    end

    module Record = struct
      module Field = struct
        let get_fields = function Record flds -> flds | _ -> []

        let get_names =
          List.map ~f:(fun (name, _) -> mk_ident (sanitize_label name))

        let gen_bind ~dir ~src (name, rep) =
          match dir with
          | Encode ->
              let bind_name = mk_ident (sanitize_label name) in
              let mods = Binding_mod.local ~mut:Mutable ~var:Invariant ()
              and type_sig = Some Foreign_type.(Atom (TPrim PrimString))
              and initial_value =
                let field =
                  match rep with
                  | AbstractPadding n -> int_to_fv n
                  | AbstractVoid -> int_to_fv 0
                  | _ -> Foreign_val.(UnaryOp (Access (Field bind_name), src))
                in
                Some (gen_transcoder_call ~dir rep ~on:field)
              in
              DeclareLocal { bind_name; mods; type_sig; initial_value }
          | Decode -> (
              match rep with
              | AbstractVoid | AbstractPadding _ ->
                  BareExpression (gen_transcoder_call ~dir ~on:src rep)
              | _ ->
                  let bind_name = mk_ident (sanitize_label name)
                  and mods =
                    Binding_mod.(local ~mut:Immutable ~var:Invariant ())
                  and type_sig = Some (convert_abstract_type rep)
                  and initial_value =
                    Some (gen_transcoder_call ~dir ~on:src rep)
                  in
                  DeclareLocal { bind_name; mods; type_sig; initial_value })
      end

      let typedec ~qual ~base_name flds =
        let f (fname, ftype) =
          mk_ident (sanitize_label fname), convert_abstract_type ftype
        in
        let foreign_fields = remove_padding flds |> List.map ~f in
        let definition = Foreign_type.Object foreign_fields in
        ( Binding_mod.exposed_definite,
          TypeDec { qual; base_name = mk_ident base_name; definition } )

      let encoder ~qual ~type_name ?(tagless = false) flds =
        let open Foreign_type in
        let arg_name = "val"
        and dir = Encode
        and intype =
          Atom (TRefer { qual; base_name = mk_ident type_name }) |> fun x ->
          if tagless then Atom (TComposite (CTTagless x)) else x
        and outtype = Atom (TPrim PrimString) in
        let base_name = transcoder_ident ~dir type_name
        and params = `Typed [ arg_name, Mandatory, intype ]
        and rhs_sig = Some outtype
        and rhs =
          let names = Field.get_names flds
          and binds =
            List.map flds ~f:(Field.gen_bind ~dir ~src:(name_to_var arg_name))
          in
          let ret = Foreign_val.ConcatStrings (List.map ~f:name_to_var names) in
          binds @ [ Return (Some ret) ]
        in
        ( Binding_mod.lambda,
          NamedLambda { qual; base_name; params; rhs_sig; rhs } )

      let decoder ~qual ~type_name ?(tagless = false) flds =
        let open Foreign_type in
        let arg_name = "input"
        and dir = Decode
        and intype = Atom (TPrim PrimDecoderInput)
        and outtype =
          Atom (TRefer { qual; base_name = mk_ident type_name }) |> fun x ->
          if tagless then Atom (TComposite (CTTagless x)) else x
        in
        let base_name = transcoder_ident ~dir type_name
        and params = `Typed [ arg_name, Mandatory, intype ]
        and rhs_sig = Some outtype
        and rhs =
          let parser_bind = "p" in
          let names = remove_padding flds |> Field.get_names
          and bindp =
            let bind_name = parser_bind
            and mods = Binding_mod.local ~mut:Binding_mod.Mutable ()
            and type_sig = Some (Atom (TPrim PrimParser))
            and initial_value =
              Some
                (Foreign_val.CreateParser { args = [ name_to_var arg_name ] })
            in
            DeclareLocal { bind_name; mods; type_sig; initial_value }
          and binds =
            List.map
              flds
              ~f:(Field.gen_bind ~dir ~src:(name_to_var parser_bind))
          in
          let ret =
            let obj =
              let explicit = List.map names ~f:(fun x -> NamedPun x) in
              Foreign_val.LitObject { explicit; implicit = None }
            in
            Some Foreign_val.(Pure (VLit obj))
          in
          bindp :: binds @ [ Return ret ]
        in
        ( Binding_mod.lambda,
          NamedLambda { qual; base_name; params; rhs_sig; rhs } )
    end

    (* ANCHOR - Gen.Simple.convert *)
    let convert ~qual ~base_name = function
      | Anon typ ->
          let typedec = Anon.typedec ~qual ~base_name typ
          and encoder = Anon.encoder ~qual ~type_name:base_name typ
          and decoder = Anon.decoder ~qual ~type_name:base_name typ in
          [ typedec; encoder; decoder ]
      | Record flds ->
          let typedec = Record.typedec ~qual ~base_name flds
          and encoder = Record.encoder ~qual ~type_name:base_name flds
          and decoder = Record.decoder ~qual ~type_name:base_name flds in
          [ typedec; encoder; decoder ]
      | _ -> assert false
  end

  module Algebraic = struct
    module EnumType = struct
      let get_elems variants =
        let f = function
          | Enum_variant (mcons, ix) ->
              let cons = enum_ident ~ix mcons in
              cons, ix
          | _ -> assert false
        in
        List.rev_map ~f variants

      let typedec ~qual ~base_name variants =
        let elems = get_elems variants in
        let definition = Foreign_type.Enum elems in
        Binding_mod.exposed_definite, TypeDec { qual; base_name; definition }

      let encoder ~qual ~type_name ~(tag_size : tag_size) =
        let open Foreign_type in
        let arg_name = "val"
        and dir = Encode
        and intype = Atom (TRefer { qual; base_name = mk_ident type_name })
        and outtype = Atom (TPrim PrimString) in
        let base_name = transcoder_ident ~dir type_name
        and params = `Typed [ arg_name, Mandatory, intype ]
        and rhs_sig = Some outtype
        and rhs =
          let ret =
            let for_type =
              match tag_size with
              | `Uint8 -> Atom (TPrim (PrimInt (Unsigned, Bits8)))
              | `Uint16 -> Atom (TPrim (PrimInt (Unsigned, Bits16)))
            in
            let on_value =
              Foreign_val.(UnaryOp (Cast for_type, name_to_var arg_name))
            in
            Foreign_val.TranscoderCall
              { dir; for_type; generics = None; args = [ [ on_value ] ] }
          in
          [ Return (Some ret) ]
        in
        ( Binding_mod.lambda,
          NamedLambda { qual; base_name; params; rhs_sig; rhs } )

      let decoder ~qual ~type_name ~(tag_size : tag_size) variants =
        let open Foreign_type in
        let arg_name = "input"
        and dir = Decode
        and intype = Atom (TPrim PrimDecoderInput)
        and outtype = Atom (TRefer { qual; base_name = mk_ident type_name }) in
        let base_name = transcoder_ident ~dir type_name
        and params = `Typed [ arg_name, Mandatory, intype ]
        and rhs_sig = Some outtype in
        let rhs =
          let bind_name = "ret" in
          let for_type =
            match tag_size with
            | `Uint8 -> Atom (TPrim (PrimInt (Unsigned, Bits8)))
            | `Uint16 -> Atom (TPrim (PrimInt (Unsigned, Bits16)))
          in
          let bind_ret =
            let initial_value =
              let on_value = name_to_var arg_name in
              Some
                (Foreign_val.TranscoderCall
                   { dir; for_type; generics = None; args = [ [ on_value ] ] })
            and type_sig = Some for_type
            and mods = Binding_mod.(local ~mut:Immutable ~var:Invariant ()) in
            DeclareLocal { bind_name; mods; type_sig; initial_value }
          and cond_ret =
            let test_expr =
              let candidate = name_to_var bind_name
              and enum_qual = qual
              and enum_base_name = type_name
              and enum_values = get_elems variants in
              Foreign_val.(
                TestExpr
                  (ValidAsEnum
                     { candidate; enum_qual; enum_base_name; enum_values }))
            and true_branch = [ Return (Some (name_to_var bind_name)) ]
            and false_branch =
              [
                ThrowError
                  (string_to_lit
                  @@ Printf.sprintf
                       "%s: illegal value for enum %s"
                       base_name
                       type_name);
              ]
            in
            IfThenElse { test_expr; true_branch; false_branch }
          in
          [ bind_ret; cond_ret ]
        in
        ( Binding_mod.lambda,
          NamedLambda { qual; base_name; params; rhs_sig; rhs } )
    end

    module ADT = struct
      module VariantType = struct
        let generate ~qual ~base_name ~cons ~tag_val ~tag_size ~tag_type_name
            ?typ () =
          let tag_name, subtype_name =
            match cons with
            | None ->
                let n = Int.to_string tag_val in
                "Tag" ^ n, mk_ident base_name ^ n
            | Some name -> mk_ident name, mk_ident base_name ^ mk_ident name
          in
          let enum_elem = tag_name, tag_val in
          let raw_subtype =
            match typ with
            | Some typ -> Simple.cast_record ~ix:tag_val typ
            | None -> Simple.cast_record (Anon AbstractVoid)
          in
          let tagged_subtype =
            Simple.add_tag
              ~tag_type_name:(mk_ident tag_type_name)
              ~tag_size
              ~tag_name
              tag_val
              raw_subtype
          in
          let typedec =
            let flds = Simple.Record.Field.get_fields tagged_subtype
            and base_name = subtype_name in
            Simple.Record.typedec ~qual ~base_name flds
          and encoder =
            let flds = Simple.Record.Field.get_fields raw_subtype
            and type_name = subtype_name in
            Simple.Record.encoder ~qual ~type_name ~tagless:true flds
          and decoder =
            let flds = Simple.Record.Field.get_fields raw_subtype
            and type_name = subtype_name in
            Simple.Record.decoder ~qual ~type_name ~tagless:true flds
          in
          subtype_name, enum_elem, [ typedec; encoder; decoder ]
      end

      let generate ~qual ~base_name ~tag_size variants ~ctxt =
        let ctxt', qual =
          (* FIXME[epic=critical] - figure out the proper logic for what
             association to add *)
          let qual' =
            Qualification.amalgamate qual (mk_ns ~prefix:true base_name)
          in
          let ctxt' =
            Context.add_assoc
              (Ident.Scope.Type_name base_name :: qual)
              (Ident.Scope.Type_name base_name :: qual')
              ctxt
          in
          ctxt', qual'
        in
        let tag_type_name = tag_ident base_name in
        let f (subtype_names, enum_elems, declarations) var =
          let new_subtype_name, new_enum_elem, new_declarations =
            match var with
            | Enum_variant (cons, tag_val) ->
                VariantType.generate
                  ~qual
                  ~base_name
                  ~cons
                  ~tag_val
                  ~tag_size
                  ~tag_type_name
                  ()
            | Type_variant (cons, tag_val, typ) ->
                VariantType.generate
                  ~qual
                  ~base_name
                  ~cons
                  ~tag_val
                  ~tag_size
                  ~tag_type_name
                  ~typ
                  ()
          in
          ( new_subtype_name :: subtype_names,
            new_enum_elem :: enum_elems,
            new_declarations @ declarations )
        in
        let subtypes, enum_elems, declarations =
          List.fold ~init:([], [], []) ~f variants
        in
        let tag_dec =
          let base_name = tag_type_name
          and definition = Foreign_type.Enum enum_elems in
          Binding_mod.exposed_definite, TypeDec { qual; base_name; definition }
        and adt_typedec =
          let f var = Foreign_type.(Atom (TRefer { qual; base_name = var })) in
          let definition = Foreign_type.Union (List.map subtypes ~f) in
          Binding_mod.exposed_definite, TypeDec { qual; base_name; definition }
        and assoc ~dir =
          let f subtype (_, tag_val) =
            tag_val, Ident.Binding.Variable_name (transcoder_ident ~dir subtype)
          in
          List.map2_exn subtypes enum_elems ~f
        in
        let adt_encoder =
          let dir = Encode and arg_name = "val" in
          let rhs =
            let arg =
              Foreign_val.(
                Pure (VPrim (PrimVariantTranscoderParamValue (assoc ~dir))))
            and f = transcoder_ident ~dir "variant"
            and tag_size = Foreign_val.(Pure (VPrim (PrimTagSize tag_size))) in
            let args = [ [ arg ]; [ tag_size ]; [ name_to_var arg_name ] ]
            and generics =
              Some
                Foreign_type.
                  [
                    Atom (TRefer { qual = []; base_name = tag_type_name });
                    Atom (TRefer { qual = []; base_name });
                  ]
            in
            [
              Return
                (Some Foreign_val.(FunApp { qual = []; f; generics; args }));
            ]
          and base_name = transcoder_ident ~dir base_name
          and intype = Foreign_type.(Atom (TRefer { qual; base_name }))
          and outtype = Foreign_type.(Atom (TPrim PrimString)) in
          let params = `Typed [ arg_name, Mandatory, intype ]
          and rhs_sig = Some outtype in
          ( Binding_mod.lambda,
            NamedLambda { qual; base_name; params; rhs_sig; rhs } )
        and adt_decoder =
          let dir = Decode and arg_name = "input" in
          let rhs =
            let arg =
              Foreign_val.(
                Pure (VPrim (PrimVariantTranscoderParamValue (assoc ~dir))))
            and f = transcoder_ident ~dir "variant"
            and tag_size = Foreign_val.(Pure (VPrim (PrimTagSize tag_size))) in
            let args = [ [ arg ]; [ tag_size ]; [ name_to_var arg_name ] ]
            and generics =
              Some
                Foreign_type.
                  [
                    Atom (TRefer { qual = []; base_name = tag_type_name });
                    Atom (TRefer { qual = []; base_name });
                  ]
            in
            [
              Return
                (Some Foreign_val.(FunApp { qual = []; f; generics; args }));
            ]
          and base_name = transcoder_ident ~dir base_name
          and intype = Foreign_type.(Atom (TPrim PrimDecoderInput))
          and outtype = Foreign_type.(Atom (TRefer { qual; base_name })) in
          let params = `Typed [ arg_name, Mandatory, intype ]
          and rhs_sig = Some outtype in
          ( Binding_mod.lambda,
            NamedLambda { qual; base_name; params; rhs_sig; rhs } )
        in
        let contents =
          tag_dec :: declarations @ [ adt_typedec; adt_encoder; adt_decoder ]
        in
        ( ctxt',
          [
            ( Binding_mod.exposed_definite,
              Define_namespace
                {
                  namespace_name = Ident.Scope.to_string (List.last_exn qual);
                  contents;
                } );
          ] )
    end
  end
end

let convert_enum_type ~qual ~base_name ~(tag_size : tag_size) variants =
  let typedec = Gen.Algebraic.EnumType.typedec ~qual ~base_name variants
  and encoder =
    Gen.Algebraic.EnumType.encoder ~qual ~type_name:base_name ~tag_size
  and decoder =
    Gen.Algebraic.EnumType.decoder ~qual ~type_name:base_name ~tag_size variants
  in
  [ typedec; encoder; decoder ]

let convert_variants_type ~qual ~base_name ~tag_size variants =
  Gen.Algebraic.ADT.generate ~qual ~base_name ~tag_size variants

let convert_adt_type ~qual ~base_name ~(tag_size : tag_size) ~variants ~ctxt =
  let is_enum = function Enum_variant _ -> true | _ -> false in
  if List.for_all ~f:is_enum variants then
    ctxt, convert_enum_type ~qual ~base_name ~tag_size variants
  else convert_variants_type ~qual ~base_name ~tag_size ~ctxt variants

let convert_type ~name ~ctxt =
  let qualids, base_name = reformat name in
  let qual = List.map ~f:(fun x -> Ident.Scope.Inferred_scope x) qualids in
  function
  | ADT { tag_size; variants; _ } ->
      convert_adt_type ~qual ~base_name ~tag_size ~variants ~ctxt
  | x -> ctxt, Gen.Simple.convert ~qual ~base_name x

let convert_toplevel ~name ~ctxt ~rep subtypes =
  let qualids, base_name = reformat name in
  let qual = List.map ~f:(fun x -> Ident.Scope.Inferred_scope x) qualids in
  let ctxt', qual =
    let qual' =
      Qualification.amalgamate
        ~drop_proto:false
        qual
        (Gen.mk_ns ~prefix:false base_name)
    in
    ( Gen.Context.add_assoc
        (Ident.Scope.Namespace_name base_name :: qual)
        qual'
        ctxt,
      qual' )
  in
  let ctxt', rsubs =
    List.fold subtypes ~init:(ctxt', []) ~f:(fun (ctxt, decls) (name, rep) ->
        convert_type ~name ~ctxt rep |> fun (ctxt', decl) ->
        ctxt', decl :: decls)
  in
  let ctxt'', top = convert_type ~name:"t" ~ctxt:ctxt' rep in
  ( ctxt'',
    [
      ( Binding_mod.exposed_definite,
        Define_namespace
          {
            namespace_name = Ident.Scope.to_string (List.hd_exn qual);
            contents = List.(rev_append rsubs [ top ] |> concat);
          } );
    ] )
