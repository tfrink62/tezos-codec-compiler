open Abstract_type
open Codec_repr

(** Simple enum for the directions of transcoding *)
type transcoder_dir = Encode | Decode

module Ident : sig
  (** Identifier used to refer to a type *)
  type type_id = string [@@deriving sexp]

  (** Identifier used to refer to a function *)
  type fun_id = string [@@deriving sexp]

  (** Identifier used to refer to fields/labels of an object/record *)
  type lab_id = string [@@deriving sexp]

  (** Identifier used to refer to a generic or polymorphic type parameter *)
  type tparam_id = string [@@deriving sexp]

  (** Identifier used to define namespaces *)
  type namespace_id = string [@@deriving sexp]

  (** Identifier used for module-level constants *)
  type const_id = string [@@deriving sexp]

  (** Identifier used for variable names, either globally, in scoped namespaces,
      * in function argument lists, or in function bodies. *)
  type var_id = string [@@deriving sexp]

  (** Identifier used for names of individual values of an * enumerated (enum)
      type *)
  type enum_id = string [@@deriving sexp]

  module Binding : sig
    type t =
      | Const_name of const_id
      | Variable_name of var_id
      | Function_name of fun_id
      | Label_name of lab_id
      | Enum_name of enum_id
    [@@deriving sexp]

    val ( = ) : t -> t -> bool

    val ( <> ) : t -> t -> bool

    val to_string : t -> string
  end

  type binding = Binding.t [@@deriving sexp]

  module Scope : sig
    type t =
      | Type_name of type_id
      | Inferred_scope of string
      | Namespace_name of namespace_id
    [@@deriving sexp]

    val ( = ) : t -> t -> bool

    val ( <> ) : t -> t -> bool

    val to_string : t -> string

    val compare : t -> t -> int

    val map : f:(string -> string) -> t -> t
  end

  type scope = Scope.t [@@deriving sexp]

  (** Labeled union of all kinds of identifier *)
  type t =
    | Binding_name of binding
    | Scope_name of scope
    | TParam_name of tparam_id
  [@@deriving sexp]
end

module Qualification : sig
  (** Any scoping label that is prepended to the base-name of a local identifier *)
  type label = Ident.scope [@@deriving sexp]

  (** Head of list is innermost qualifier; qualification should be serialized in
      reverse. 'prefix' therefore refers any sub-list containing the final
      element, and 'suffix' to any sub-list containing the first element *)
  type t = label list [@@deriving sexp]

  (** [bisect xs ys] computes the longest common prefix [lcp] of [xs] and [ys],
      and returns [((xt,yt),lcp)] where [xs == xt @ lcp] and [ys == yt @ lcp] *)
  val bisect : t -> t -> (t * t) * t

  (** [get_rel_prefix scope target] splits [target] into its longest prefix that
      is a sub-sequence of [scope], and all remaining labels that diverge from
      [scope]. *)
  val get_rel_prefix : t -> t -> t * t

  (** [drop_inferred_prefix q] returns the longest suffix of [q] whose outermost
      label is not an [Inferred_scope] variant *)
  val drop_inferred_prefix : t -> t

  (** Reverse-order list comparison that forgetfully coerces labels to their
      underlying strings *)
  val compare : t -> t -> int

  (** [alamgamate ?drop_proto qual base] generates a [label list] consisting of
      one element, computed by accumulating the labels of qual in normalized
      (i.e. reverse) order and prepending them to [base] with intervening "__"
      seperation. If any label contains a namespace-prefix "NS", it is moved to
      the beginning of the label without duplication.

      @param [drop_proto] When set, omits any label with a prefix of "Proto"
      after scrubbing any "NS" prefix. Default [true]. *)
  val amalgamate : ?drop_proto:bool -> t -> Ident.Scope.t -> t

  (** Produces a non-identifier stringification of a [label list] that
      normalizes the order of the labels and indicates label separation with a
      ">" separator. To be used for debugging rather than construction of new
      labels. *)
  val to_string : t -> string

  (** [mk_ns ?prefix ident] wraps [ident] in a [Namespace_name] constructor,
      after optionally adding an "NS" prefix to prevent name shadowing.

      @param [prefix] When set, adds "NS" to the beginning of [ident] before
      wrapping. Default [true]. *)
  val mk_ns : ?prefix:bool -> string -> Ident.Scope.t
end

module Foreign_type : sig
  (** Primitive types we expect to appear either in the foreign language itself,
      or to have defined ourselves in the backend *)
  type prim =
    | PrimBool
    | PrimInt of (sign * bit_width)
    | PrimIntRanged of ((int * int) * byte_width)
    | PrimString
    | PrimOpaqueBytes
    | PrimFloat of float_precision
    | PrimOpaqueBytesFixedLen of int
    | PrimVariantTranscoderParamType of transcoder_dir
    | PrimWidth
    | PrimDecoderInput
    | PrimParser
    | PrimPadding of int
    | PrimZarith_N
    | PrimZarith_Z

  type type_constraint =
    | Refines  (** Signature of X is consistent with signature of Y *)
    | Reifies  (** Concrete type X is a valid instantiation of meta-type Y *)

  (** Composite types we expect to appear either in the foreign language itself,
      or to have defined ourselves in the backend *)
  type composite_type =
    | CTArray of t
    | CTOption of t
    | CTOrError of t
    | CTTuple2 of t * t
    | CTTagged of t * t
        (** [CTTagged (t1,t2)] is a derived type of [t1] that includes a tag of
            type [t2] *)
    | CTTagless of t
        (** [CTTagless t] is a derived type of [t] that excludes its tag field *)
    | CTDynWidth of width * t

  and atom =
    | TPrim of prim
    | TComposite of composite_type
    | TParam of Ident.tparam_id
    | TRefer of { qual : Qualification.t; base_name : Ident.type_id }
    | TNarrow of {
        qual : Qualification.t;
        base_name : Ident.type_id;
        narrow : Ident.enum_id;
      }
        (** A reference to an enum-type that could be narrowed to a particular
            enum-value *)
    | TRefl  (** Used for `Self` a la Rust, or `typeof this` a la TypeScript *)
    | TLiteral of string
        (** Used for type that are language-specific; if literal strings are
            allowed as types, such as in TypeScript, they must be enclosed in
            their own double-quotes *)
    | TConstrained of (t * type_constraint * t)
        (** [TConstrained (t, con, u)] is equivalent to "Type [t] is related to
            type [u] by condition [con]" *)

  and t =
    | Atom of atom
    | Object of (Ident.lab_id * t) list
    | Union of t list
    | Enum of (Ident.enum_id * int) list
end

(** Specification of how a particular field in an object literal is to be filled *)
type 'a field_fill =
  | Assign of Ident.lab_id * 'a
      (** [Assign (fld,v)] indicates that field [fld] is explictly assigned the
          value [v] *)
  | NamedPun of Ident.lab_id
      (** [NamedPun fld] indicates that the field [fld] should inherit the value
          of an in-scope binding of the same name *)

(** Specification of how multiple, unidentified fields in an object literal are
    to be filled *)
type record_wildcard =
  | LocalScoped
      (** Inherit field values from correspondingly named bindings in local
          scope *)
  | FromObject of Ident.var_id
      (** Inherit field values from another object, bound to an identifier *)

(** Specification of how an object literal is to be constructed *)
type 'a record_fill = {
  explicit : 'a field_fill list;
  implicit : record_wildcard option;
}

module Foreign_val : sig
  (** Value of a primitive (per-language) or otherwise fundamental (per-backend)
      type *)
  type prim =
    | PrimTrue
    | PrimFalse
    | PrimNum of string
        (** Any numeric value, regardless of type representation or formatting *)
    | Prim_L
        (** _L is meant to look like [⊥]; this is the bottom value (e.g. null) *)
    | PrimWidth of size_repr
    | PrimTagSize of tag_size
    | PrimVariantTranscoderParamValue of (int * Ident.binding) list
        (** Tag-indexed association list mapping to variant-specific transcoders *)

  type literal =
    | LitString of string
        (** String literals should omit their openquote and closequote
            internally, as those are language-dependent *)
    | LitArray of t list
    | LitObject of t record_fill

  and derived_term =
    | Field of Ident.lab_id
        (** Field of a structure/object with a given name *)
    | Index of t  (** Index of an array-like structure with a given index *)
    | KeyVal of t  (** Entry of a dict/map-like structure with a given key*)
    | Dereference  (** Dereferencing of a pointer-like value *)
    | Reference  (** Reference of a value into a pointer-like term *)

  and unary_op =
    | Access of derived_term
    | CallMethod of { meth : Ident.fun_id; args : t list }
    | BooleanNegate
    | Cast of Foreign_type.t

  and term =
    | VPrim of prim
    | VLit of literal
    | Refl  (** Used for `this` or `self` in languages with those keywords *)
    | Bound of Ident.binding
    | Ternary of (t * t * t)
        (** [Ternary (p, t, f)] translates to [(p) ? t : f] *)

  and test_expr =
    | ValidAsEnum of {
        candidate : t;
        enum_qual : Qualification.t;
        enum_base_name : Ident.type_id;
        enum_values : (Ident.enum_id * int) list;
      }
        (** Abstract expression which should evaluate to [true] iff [candidate]
            is a valid element of the specified enum-type, and [false]
            otherwise. *)

  and t =
    | Pure of term
    | ConstructorCall of {
        qual : Qualification.t;
        f : Ident.type_id;
        generics : Foreign_type.t list option;
        args : t list;
      }
        (** Standard constructor calling convention, e.g.
            [new MyClass(foo,bar)]; for static factory methods use FunApp
            instead *)
    | FunApp of {
        qual : Qualification.t;
        f : Ident.fun_id;
        generics : Foreign_type.t list option;
        args : t list list;
      }
        (** Application of a function to a list of lists of arguments, meant to
            be applied as argument groups in sequence *)
    | CreateParser of { args : t list }
        (** Abstract expression that produces a parser object from a list of
            arguments *)
    | TranscoderCall of {
        dir : transcoder_dir;
        for_type : Foreign_type.t;
        generics : Foreign_type.t list option;
        args : t list list;
      }
        (** Call to a transcoder function, either defined in the backend or
            generated from another part of the codec *)
    | ConcatStrings of t list  (** Used to concatenate many strings at once *)
    | UnaryOp of (unary_op * t)
    | TestExpr of test_expr
    | Paren of t  (** Parenthetical of an expression *)
end

(** Modifiers to a definition or declaration that determine what
    language-specific keywords are used. *)
module Binding_mod : sig
  (** Visibility of the binding *)
  type vis_mod =
    | Local
        (** Binding is local by definition and has no mechanism for access
            outside the current scope *)
    | Hidden
        (** Binding could be made visible outside the current scope, but is not *)
    | Exposed
        (** Binding can be referenced either directly (if global) or by external
            qualification (default) *)

  (** Mutability of the RHS value *)
  type mut_mod =
    | Immutable  (** Value cannot or will not be mutated *)
    | Mutable  (** Value may be mutated if language allows *)

  (** Variability of the LHS binding *)
  type var_mod =
    | Invariant  (** Binding cannot be updated or reassigned *)
    | Volatile  (** Binding can be updated and/or reassigned *)

  type ext_mod =
    | Definite
        (** Binding is made concrete either here or elsewhere in current scope *)
    | Proxy
        (** Binding is merely an abstract promise that a concrete definition of
            the binding will exist in scope at compile-time or run-time *)

  type t = {
    visibility : vis_mod;
    mutability : mut_mod option;
    variability : var_mod option;
    externality : ext_mod;
  }

  (** Binding modifications with visibility [Exposed] and externality
      [Definite], using the implicit per-language default to determine
      mutability and variability. *)
  val exposed_definite : t

  (** Produce a [Binding_mod.t] suitable for a local-scope variable, which
      optionally takes a [mut_mod] and [var_mod] *)
  val local : ?mut:mut_mod -> ?var:var_mod -> unit -> t

  (** Binding modifications suitable when the binding RHS is an anonymous
      function we wish to call by the LHS name *)
  val lambda : t
end

type binding_mods = Binding_mod.t

(** Positional argument to an anonymous (lambda) function *)
type lambda_arg =
  | Point of Ident.var_id * Foreign_type.t option
      (** Named argument with an optional type-signature *)

(** Modifier to a function argument *)
type 'a fun_arg_mod =
  | Mandatory
  | Default of 'a  (** If not provided, use a default value *)
  | Optional
      (** If not provided, use a sentinel not-value (e.g. undefined, None, null) *)

(** List of arguments with that are either all typed or all untyped *)
type ('t, 'v) fun_args =
  [ `Typed of (Ident.var_id * 'v fun_arg_mod * 't) list
    (** All argument have type signatures *)
  | `Untyped of (Ident.var_id * 'v fun_arg_mod) list
    (** No arguments have type signatures *) ]

type foreign_fun_args = (Foreign_type.t, Foreign_val.t) fun_args

(** Convert a [('t, 'v) fun_args] value into a list of named and annotated
    arguments with optional type signatures *)
val unify_funargs :
  ('t, 'v) fun_args -> (Ident.var_id * 'v fun_arg_mod * 't option) list

(** Convert [('t1,'v1) fun_args] to [('t2,'v2) fun_args] by a bifunctorial map
    operation

    @param ft Function that converts the type representation
    @param fv Function that converts the value representation *)
val convert_funargs :
  ft:('t1 -> 't2) ->
  fv:('v1 -> 'v2) ->
  ('t1, 'v1) fun_args ->
  ('t2, 'v2) fun_args

(** Type representing individual statements in function bodies *)
type statement =
  | DeclareLocal of {
      bind_name : Ident.var_id;
      mods : binding_mods;
      type_sig : Foreign_type.t option;
      initial_value : Foreign_val.t option;
    }
  | AssignLocal of { bind_name : Ident.var_id; new_value : Foreign_val.t }
  | IfThenElse of {
      test_expr : Foreign_val.t;
      true_branch : statement list;
      false_branch : statement list;
    }
  | BareExpression of Foreign_val.t
  | Return of Foreign_val.t option
  | ThrowError of Foreign_val.t

(** Type representing a top-level declaration of type, value, namespace, or
    function *)
type declaration =
  | Define_namespace of {
      namespace_name : Ident.namespace_id;
      contents : (binding_mods * declaration) list;
    }
  | TypeDec of {
      qual : Qualification.t;
      base_name : Ident.type_id;
      definition : Foreign_type.t;
    }
  | NamedLambda of {
      qual : Qualification.t;
      base_name : Ident.var_id;
      params : foreign_fun_args;
      rhs_sig : Foreign_type.t option;
      rhs : statement list;
    }
  | Function of {
      qual : Qualification.t;
      base_name : Ident.fun_id;
      params : foreign_fun_args;
      return_type : Foreign_type.t option;
      body : statement list;
    }

type directive =
  | ImportAll of { qualified_name : string option; source_file : string }
  | ImportBindings of { bindings : string list; source_file : string }

type source_file = {
  filename : string;
  preamble : directive list;
  contents : (binding_mods * declaration) list;
}

type foreign_codebase =
  | SingleFile of source_file
  | ManyFile of source_file list
