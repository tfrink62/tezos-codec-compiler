open Foreign_ast
open Foreign_converter
open Language_repr
open Abstract_type

val unimplemented : unit -> 'a

(** Internal state passed between combinators in the [Producer] functor, used
    for determining the appropriate structural qualification of generated
    identifiers based on accumulated rescoping rules and the current
    namespace-stack. *)
module ProducerContext : sig
  type t = { scope : Qualification.t; rescopes : Gen.Context.t }

  (** Adjust the context to reflect entry into a namespace of the given name *)
  val enter_namespace : Ident.namespace_id -> t -> t

  (** Adjust the context to reflect exit of the most recently entered namespace *)
  val leave_namespace : t -> t

  (** Adjust the context to prune local rescopes to avoid histeresis *)
  val reset_local : t -> t

  (** Generate a 'fresh' context for first-pass production *)
  val fresh : unit -> t

  (** Impute the proper qualification of an opaque named reference to a type
      based on the accumulated rescoping rules and the current namespace-stack

      @param debug When true, prints non-identity requalifications to stderr *)
  val requalify :
    base_name:Ident.type_id ->
    qual:Qualification.t ->
    ctxt:t ->
    ?debug:bool ->
    unit ->
    string * Qualification.t
end

(** Functor for generating sourcecode in a particular backend language from
    foreign-code AST *)
module Producer : functor (L : Backend) -> sig
  (** Producer-context that is referred to and potentially updated by a
      production method *)
  type ctxt = ProducerContext.t

  (** [('a,'b) t] is the type of a function that converts a value of ['a] to a
      value of type ['b] in a context-aware and context-mutating fashion *)
  type ('a, 'b) t = ctxt:ctxt -> 'a -> ctxt * 'b

  (** Sequentially apply a production rule to each value in a list, using the
      returned context of each element as the input context for the following
      element; the context returned is the context returned after producing the
      last element of the list *)
  val seq : ('a, 'b) t -> ('a list, 'b list) t

  (** [map ~f prod ~ctxt x] applies [f] to the return value of [prod ~ctxt x],
      leaving the output context unchanged *)
  val map : f:('b -> 'c) -> ('a, 'b) t -> ('a, 'c) t

  (** [opt prod ~ctxt x] applies [prod ~ctxt] to [x] of type ['a option] and
      returns [ctxt * 'b option], in a similar manner as [Option.map] *)
  val opt : ('a, 'b) t -> ('a option, 'b option) t

  (** [ign prod ~ctxt x] drops the returned context of [prod ~ctxt x] and
      returns only the converted value *)
  val ign : ('a, 'b) t -> ctxt:ctxt -> 'a -> 'b

  (** [tup3] is a variant of [seq] specialized or 3-tuples rather than lists of
      arbitrary length; like [seq], it threads the returned context through each
      successive operation, rather than computing each production using the same
      fixed initial context. *)
  val tup3 : ('a, 'b) t -> ('a * 'a * 'a, 'b * 'b * 'b) t

  val produce_directive : (directive, string) t

  val produce_type : (Foreign_type.t, string) t

  (** Foreign source-code production rule for typed fields *)
  val produce_field : (string * Foreign_type.t, string * string) t

  (** Foreign source-code production rule for named enum elements *)
  val produce_elem : (string * int, string * string) t

  (** Foreign source-code production rule for type definitions *)
  val produce_typedef : (Foreign_type.t, contents) t

  (** Foreign source-code production rule for function arguments *)
  val produce_param :
    (string * 'a fun_arg_mod * Foreign_type.t option, string * string option) t

  val produce_value : (Foreign_val.t, string) t

  val produce_statement : (statement, string) t

  (** Foreign source-code production rule for declarations *)
  val produce_declaration : (binding_mods * declaration, string) t

  val produce_sourcefile : (source_file, string list) t

  val produce_codebase : (foreign_codebase, string list) t
end

(** Converts a toplevel type and a list of named sub-types into a pair
    consisting of a potential filename and the source-code for the definitions
    and transcoders for all provided types according to a particular [Backend]
    language module and with respect to a particular [ProducerContext.t]. Also
    returns the updated context accumulated from the recursive production of the
    source-code from the specified types. Optionally takes a directive list to
    prepend to the content, which defaults to the standard preamble associated
    with the [Backend] in question. *)
val gen_all :
  (module Backend) ->
  ctxt:ProducerContext.t ->
  ?preamble:directive list ->
  (string * typrep) * (string * typrep) list ->
  ProducerContext.t * (string * string list)
