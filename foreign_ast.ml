open Abstract_type
open Codec_repr
open Core

(** Simple enum for the directions of transcoding *)
type transcoder_dir = Encode | Decode

module Ident = struct
  type type_id = string [@@deriving sexp]

  type fun_id = string [@@deriving sexp]

  type lab_id = string [@@deriving sexp]

  type tparam_id = string [@@deriving sexp]

  type namespace_id = string [@@deriving sexp]

  type const_id = string [@@deriving sexp]

  type var_id = string [@@deriving sexp]

  type enum_id = string [@@deriving sexp]

  module Binding = struct
    type t =
      | Const_name of const_id
      | Variable_name of var_id
      | Function_name of fun_id
      | Label_name of lab_id
      | Enum_name of enum_id
    [@@deriving sexp]

    let ( = ) x y =
      match x, y with
      | Const_name id1, Const_name id2
      | Variable_name id1, Variable_name id2
      | Function_name id1, Function_name id2
      | Label_name id1, Label_name id2
      | Enum_name id1, Enum_name id2 ->
          String.(id1 = id2)
      | _ -> false

    let ( <> ) x y =
      match x, y with
      | Const_name id1, Const_name id2
      | Variable_name id1, Variable_name id2
      | Function_name id1, Function_name id2
      | Label_name id1, Label_name id2
      | Enum_name id1, Enum_name id2 ->
          String.(id1 <> id2)
      | _ -> true

    let to_string = function
      | Const_name id
      | Variable_name id
      | Function_name id
      | Label_name id
      | Enum_name id ->
          id
  end

  type binding = Binding.t [@@deriving sexp]

  module Scope = struct
    type t =
      | Type_name of type_id
      | Inferred_scope of string
      | Namespace_name of namespace_id
    [@@deriving sexp]

    let ( = ) x y =
      match x, y with
      | Type_name id1, Type_name id2
      | Inferred_scope id1, Inferred_scope id2
      | Namespace_name id1, Namespace_name id2 ->
          String.(id1 = id2)
      | _ -> false

    let ( <> ) x y =
      match x, y with
      | Type_name id1, Type_name id2
      | Inferred_scope id1, Inferred_scope id2
      | Namespace_name id1, Namespace_name id2 ->
          String.(id1 <> id2)
      | _ -> true

    let to_string = function
      | Type_name s | Inferred_scope s | Namespace_name s -> s

    let compare x y = String.compare (to_string x) (to_string y)

    let map ~f = function
      | Type_name id -> Type_name (f id)
      | Inferred_scope id -> Inferred_scope (f id)
      | Namespace_name id -> Namespace_name (f id)
  end

  type scope = Scope.t [@@deriving sexp]

  type t =
    | Binding_name of binding
    | Scope_name of scope
    | TParam_name of tparam_id
  [@@deriving sexp]
end

module Qualification = struct
  type label = Ident.scope [@@deriving sexp]

  type t = label list [@@deriving sexp]

  let to_string xs =
    String.concat ~sep:">" @@ List.rev_map ~f:Ident.Scope.to_string xs

  let bisect xs ys =
    let rxs = List.rev xs and rys = List.rev ys in
    let rxys, rrem = List.zip_with_remainder rxs rys in
    let rcommon, rdiff =
      List.split_while ~f:(fun (x, y) -> Ident.Scope.(x = y)) rxys
    in
    let lcp = List.rev @@ List.map rcommon ~f:(fun (x, _) -> x)
    and rx_uniq, ry_uniq = List.unzip rdiff in
    let x_suff, y_suff =
      match rrem with
      | None -> List.rev rx_uniq, List.rev ry_uniq
      | Some (Either.First rxt) ->
          List.rev_append rxt (List.rev rx_uniq), List.rev ry_uniq
      | Some (Either.Second ryt) ->
          List.rev rx_uniq, List.rev_append ryt (List.rev ry_uniq)
    in
    (x_suff, y_suff), lcp

  let drop_inferred_prefix (q : t) : t =
    let qrev = List.rev q in
    List.rev
    @@ List.drop_while
         ~f:(function Ident.Scope.Inferred_scope _ -> true | _ -> false)
         qrev

  let drop_proto (q : t) : t =
    let f (prefix, accum) lab =
      match Ident.Scope.to_string lab with
      | x when String.is_prefix ~prefix:"NSProto" x -> true, accum
      | x when String.is_prefix ~prefix:"NS" x ->
          ( true,
            Ident.Scope.map ~f:(fun x -> String.drop_prefix x 2) lab :: accum )
      | x when String.is_prefix ~prefix:"Proto" x -> prefix, accum
      | _ -> prefix, lab :: accum
    in
    let prefix, labels = List.fold ~f q ~init:(false, []) in
    List.rev_mapi
      ~f:(fun ix lab ->
        if ix = 0 && prefix then Ident.Scope.map ~f:(fun lab -> "NS" ^ lab) lab
        else lab)
      labels

  let get_rel_prefix (scope : t) (target : t) : t * t =
    (* REVIEW[epic=stopgap] - drop module-level namespace to fix mismatch *)
    let _toplevel = drop_inferred_prefix scope in
    let scope = List.drop_last_exn scope in
    let (_in_scope, in_target), common = bisect scope (drop_proto target) in
    (* Stdio.eprintf "%s<>%s -> (%s){%s,%s}\n" (to_string scope) (to_string
       target) (to_string common) (to_string _in_scope) (to_string in_target); *)
    in_target, common

  let mk_ns ?(prefix = true) id =
    Ident.Scope.Namespace_name (if prefix then "NS" ^ id else id)

  let amalgamate ?(drop_proto = true) (q : t) base_name : t =
    let f (prefix, accum) = function
      | x
        when String.is_prefix ~prefix:"NSProto" (Ident.Scope.to_string x)
             && drop_proto ->
          true, accum
      | x when String.is_prefix ~prefix:"NS" (Ident.Scope.to_string x) ->
          true, String.drop_prefix (Ident.Scope.to_string x) 2 :: accum
      | x
        when String.is_prefix ~prefix:"Proto" (Ident.Scope.to_string x)
             && drop_proto ->
          prefix, accum
      | x -> prefix, Ident.Scope.to_string x :: accum
    in
    let prefix, labels = List.fold ~f q ~init:(f (false, []) base_name) in
    let unilabel = String.concat ~sep:"__" labels in
    [ mk_ns ~prefix unilabel ]

  let compare xs ys =
    List.compare Ident.Scope.compare (List.rev xs) (List.rev ys)
end

module Foreign_type = struct
  type prim =
    | PrimBool
    | PrimInt of (sign * bit_width)
    | PrimIntRanged of ((int * int) * byte_width)
    | PrimString
    | PrimOpaqueBytes
    | PrimFloat of float_precision
    | PrimOpaqueBytesFixedLen of int
    | PrimVariantTranscoderParamType of transcoder_dir
    | PrimWidth
    | PrimDecoderInput
    | PrimParser
    | PrimPadding of int
    | PrimZarith_N
    | PrimZarith_Z

  type type_constraint = Refines | Reifies

  type composite_type =
    | CTArray of t
    | CTOption of t
    | CTOrError of t
    | CTTuple2 of t * t
    | CTTagged of t * t
    | CTTagless of t
    | CTDynWidth of width * t

  and atom =
    | TPrim of prim
    | TComposite of composite_type
    | TParam of Ident.tparam_id
    | TRefer of { qual : Qualification.t; base_name : Ident.type_id }
    | TNarrow of {
        qual : Qualification.t;
        base_name : Ident.type_id;
        narrow : Ident.enum_id;
      }
    | TRefl
    | TLiteral of string
    | TConstrained of (t * type_constraint * t)

  and t =
    | Atom of atom
    | Object of (Ident.lab_id * t) list
    | Union of t list
    | Enum of (Ident.enum_id * int) list
end

type 'a field_fill = Assign of Ident.lab_id * 'a | NamedPun of Ident.lab_id

type record_wildcard = LocalScoped | FromObject of Ident.var_id

type 'a record_fill = {
  explicit : 'a field_fill list;
  implicit : record_wildcard option;
}

module Foreign_val = struct
  type prim =
    | PrimTrue
    | PrimFalse
    | PrimNum of string
    | Prim_L
    | PrimWidth of size_repr
    | PrimTagSize of tag_size
    | PrimVariantTranscoderParamValue of (int * Ident.binding) list

  type literal =
    | LitString of string
        (** String literals should omit their openquote and closequote
            internally, as those are language-dependent *)
    | LitArray of t list
    | LitObject of t record_fill

  and derived_term =
    | Field of Ident.lab_id
        (** Field of a structure/object with a given name *)
    | Index of t  (** Index of an array-like structure with a given index *)
    | KeyVal of t  (** Entry of a dict/map-like structure with a given key*)
    | Dereference  (** Dereferencing of a pointer-like value *)
    | Reference  (** Reference of a value into a pointer-like term *)

  and unary_op =
    | Access of derived_term
    | CallMethod of { meth : Ident.fun_id; args : t list }
    | BooleanNegate
    | Cast of Foreign_type.t

  and term =
    | VPrim of prim
    | VLit of literal
    | Refl
    | Bound of Ident.binding
    | Ternary of (t * t * t)

  and test_expr =
    | ValidAsEnum of {
        candidate : t;
        enum_qual : Qualification.t;
        enum_base_name : Ident.type_id;
        enum_values : (Ident.enum_id * int) list;
      }

  and t =
    | Pure of term
    | ConstructorCall of {
        qual : Qualification.t;
        f : Ident.type_id;
        generics : Foreign_type.t list option;
        args : t list;
      }
    | FunApp of {
        qual : Qualification.t;
        f : Ident.fun_id;
        generics : Foreign_type.t list option;
        args : t list list;
      }
    | CreateParser of { args : t list }
    | TranscoderCall of {
        dir : transcoder_dir;
        for_type : Foreign_type.t;
        generics : Foreign_type.t list option;
        args : t list list;
      }
    | ConcatStrings of t list
    | UnaryOp of (unary_op * t)
    | TestExpr of test_expr
    | Paren of t
end

module Binding_mod = struct
  type vis_mod = Local | Hidden | Exposed

  type mut_mod = Immutable | Mutable

  type var_mod = Invariant | Volatile

  type ext_mod = Definite | Proxy

  type t = {
    visibility : vis_mod;
    mutability : mut_mod option;
    variability : var_mod option;
    externality : ext_mod;
  }

  let exposed_definite =
    {
      visibility = Exposed;
      mutability = None;
      variability = None;
      externality = Definite;
    }

  let local ?mut ?var () =
    {
      visibility = Local;
      mutability = mut;
      variability = var;
      externality = Definite;
    }

  let lambda =
    {
      visibility = Exposed;
      mutability = Some Immutable;
      variability = Some Invariant;
      externality = Definite;
    }
end

type binding_mods = Binding_mod.t

type lambda_arg = Point of Ident.var_id * Foreign_type.t option

type 'a fun_arg_mod = Mandatory | Default of 'a | Optional

type ('t, 'v) fun_args =
  [ `Typed of (Ident.var_id * 'v fun_arg_mod * 't) list
  | `Untyped of (Ident.var_id * 'v fun_arg_mod) list ]

type foreign_fun_args = (Foreign_type.t, Foreign_val.t) fun_args

let unify_funargs = function
  | `Typed ts -> List.map ts ~f:(fun (x, y, z) -> x, y, Some z)
  | `Untyped us -> List.map us ~f:(fun (x, y) -> x, y, None)

let convert_funargs ~(ft : 't1 -> 't2) ~(fv : 'v1 -> 'v2) :
    ('t1, 'v1) fun_args -> ('t2, 'v2) fun_args = function
  | `Typed ts ->
      `Typed
        (List.map ts ~f:(function
            | id, ((Mandatory | Optional) as m), t -> id, m, ft t
            | id, Default v, t -> id, Default (fv v), ft t))
  | `Untyped us ->
      `Untyped
        (List.map us ~f:(function
            | (_, (Mandatory | Optional)) as x -> x
            | id, Default v -> id, Default (fv v)))

type statement =
  | DeclareLocal of {
      bind_name : Ident.var_id;
      mods : binding_mods;
      type_sig : Foreign_type.t option;
      initial_value : Foreign_val.t option;
    }
  | AssignLocal of { bind_name : Ident.var_id; new_value : Foreign_val.t }
  | IfThenElse of {
      test_expr : Foreign_val.t;
      true_branch : statement list;
      false_branch : statement list;
    }
  | BareExpression of Foreign_val.t
  | Return of Foreign_val.t option
  | ThrowError of Foreign_val.t

type declaration =
  | Define_namespace of {
      namespace_name : Ident.namespace_id;
      contents : (binding_mods * declaration) list;
    }
  | TypeDec of {
      qual : Qualification.t;
      base_name : Ident.type_id;
      definition : Foreign_type.t;
    }
  | NamedLambda of {
      qual : Qualification.t;
      base_name : Ident.var_id;
      params : foreign_fun_args;
      rhs_sig : Foreign_type.t option;
      rhs : statement list;
    }
  | Function of {
      qual : Qualification.t;
      base_name : Ident.fun_id;
      params : foreign_fun_args;
      return_type : Foreign_type.t option;
      body : statement list;
    }

type directive =
  | ImportAll of { qualified_name : string option; source_file : string }
  | ImportBindings of { bindings : string list; source_file : string }

type source_file = {
  filename : string;
  preamble : directive list;
  contents : (binding_mods * declaration) list;
}

type foreign_codebase =
  | SingleFile of source_file
  | ManyFile of source_file list
