open Codec_repr
open Json_ast
open Core

exception Empty_input_json_array

exception Illegal_json_input of Yojson.Safe.t

exception Nested_sequence

let nullish ks =
  let f = function "title" | "description" | "enum" -> false | _ -> true in
  List.is_empty @@ List.filter ~f ks

let unexpected_case case_type invalid =
  failwith
  @@ Printf.sprintf
       "Unsupported value for conversion to AST type `%s`: '%s'"
       case_type
       invalid

let to_string_list_option =
  let open Yojson.Safe.Util in
  function
  | `Null -> None
  | json -> json |> to_list |> List.map ~f:to_string |> Option.some

let to_datakind_spec json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  match json |> member "kind" |> to_string with
  | "Variable" -> VariableKind
  | "Dynamic" -> DynamicKind
  | "Float" -> FloatKind (json |> member "size" |> to_int)
  | other -> unexpected_case "datakind_spec" other

let to_subtype_desc json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  let title = json |> member "title" |> to_string
  and _description = json |> member "description" |> to_string_option in
  { title; _description }

let rec to_layout_spec ?(depth = 0) json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  match json |> member "kind" |> to_string with
  | "Zero_width" -> `Zero_width
  | "Int" -> `Int (json |> member "size" |> to_string |> Int_size.of_string_exn)
  | "Bool" -> `Bool
  | "RangedInt" ->
      let min = json |> member "min" |> to_int
      and max = json |> member "max" |> to_int in
      `RangedInt (min, max)
  | "Float" -> `Float
  | "Bytes" -> `Bytes
  | "String" -> `String
  | "Seq" when depth = 0 ->
      `Seq (Layout.narrow (to_layout_spec ~depth:1 (json |> member "layout")))
  | "Seq" -> raise Nested_sequence
  | "Enum" ->
      let size = json |> member "size" |> to_string |> Int_size.of_string_exn
      and reference = json |> member "reference" |> to_string in
      `Enum (size, reference)
  | "Ref" -> `Ref (json |> member "name" |> to_string)
  | "Padding" -> `Padding
  | unhandled -> failwith ("unhandled layout spec " ^ unhandled)

let to_size_repr = function
  | "Uint8" -> `Uint8
  | "Uint30" -> `Uint30
  | other ->
      failwith
      @@ Printf.sprintf "No variant exists for size_repr \"%s\"\n" other

let to_tag_size = function
  | "Uint8" -> `Uint8
  | "Uint16" -> `Uint16
  | other ->
      failwith @@ Printf.sprintf "No variant exists for tag_size \"%s\"\n" other

let to_field_spec json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  match json |> member "kind" |> to_string with
  | "dyn" ->
      let num_fields = json |> member "num_fields" |> to_int
      and name = json |> member "name" |> to_string_option
      and width =
        match json |> member "size" |> to_string with
        | "Uint8" -> `Uint8
        | "Uint30" -> `Uint30
        | other -> unexpected_case "width" other
      in
      DynField { name; num_fields; width }
  | "named" ->
      let name = json |> member "name" |> to_string
      and layout = json |> member "layout" |> to_layout_spec
      and data_kind = json |> member "data_kind" |> to_datakind_spec in
      NamedField { name; layout; data_kind }
  | "anon" ->
      let layout = json |> member "layout" |> to_layout_spec
      and data_kind = json |> member "data_kind" |> to_datakind_spec in
      AnonField { layout; data_kind }
  | "option_indicator" ->
      let name = json |> member "name" |> to_string in
      OptionalField { name }
  | other -> unexpected_case "kind" other

let to_tagtype_repr json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  let tag = json |> member "tag" |> to_int
  and fields = json |> member "fields" |> to_list |> List.map ~f:to_field_spec
  and name = json |> member "name" |> to_string_option in
  { tag; fields; name }

let to_enumtype_repr json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  match json |> to_list with
  | [ i; s ] ->
      let enum_val = i |> to_int and enum_tag = s |> to_string in
      enum_tag, enum_val
  | _ -> assert false

let to_subtype_encoding json =
  let open Yojson.Safe.Util in
  let characteristic = function
    | "tag_size" -> true
    | "fields" -> true
    | "size" -> true
    | _ -> false
  in
  match json |> keys |> List.find ~f:characteristic with
  | Some "tag_size" ->
      let tag_size = json |> member "tag_size" |> to_string |> to_tag_size
      and kind = json |> member "kind" |> to_datakind_spec
      and cases =
        json |> member "cases" |> to_list |> List.map ~f:to_tagtype_repr
      in
      Tagged { tag_size; kind; cases }
  | Some "fields" ->
      let fields =
        json |> member "fields" |> to_list |> List.map ~f:to_field_spec
      in
      Fielded { fields }
  | Some "size" ->
      let size = json |> member "size" |> to_string |> to_size_repr
      and cases =
        json |> member "cases" |> to_list |> List.map ~f:to_enumtype_repr
      in
      Enumerated { size; cases }
  | _ -> failwith @@ Yojson.Safe.to_string json

let to_subtype_spec json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  let desc = json |> member "description" |> to_subtype_desc
  and encoding = json |> member "encoding" |> to_subtype_encoding in
  { desc; encoding }

let to_type_spec json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  json |> member "fields" |> to_list |> List.map ~f:to_field_spec
  |> fun fields -> Fielded { fields }

let to_binary_spec json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  let toplevel = json |> member "toplevel" |> to_type_spec
  and subtypes =
    json |> member "fields" |> to_list |> List.map ~f:to_subtype_spec
  in
  { toplevel; subtypes }

let rec to_schema_kind json : schema_kind =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  let characteristic = function
    | "$ref" -> true
    | "type" -> true
    | "oneOf" -> true
    | _ -> false
  in
  match json |> keys |> List.find ~f:characteristic with
  | Some "$ref" ->
      let target = json |> member "$ref" |> to_string |> to_ident in
      `Ref target
  | Some "type" -> (
      match json |> member "type" |> to_string with
      | "integer" ->
          let minimum = json |> member "minimum" |> to_int_option
          and maximum = json |> member "maximum" |> to_int_option in
          `Integer { minimum; maximum }
      | "number" ->
          let minimum = json |> member "minimum" |> to_int_option
          and maximum = json |> member "maximum" |> to_int_option in
          `Number { minimum; maximum }
      | "object" ->
          let properties =
            json |> member "properties" |> to_assoc
            |> List.map ~f:(fun (x, raw) -> x, to_schema_element raw)
          and required = json |> member "required" |> to_string_list_option
          and additional_properties =
            json |> member "additionalProperties" |> to_bool_option
          in
          `Object { properties; required; additional_properties }
      | "string" ->
          let pattern = json |> member "pattern" |> to_string_option in
          `String { pattern }
      | "array" ->
          let items = json |> member "items" |> to_schema_array in
          `Array items
      | "boolean" -> `Boolean
      | "null" -> `Null
      | other -> unexpected_case "schema_type" other)
  | Some "oneOf" ->
      `Union (json |> member "oneOf" |> to_list |> List.map ~f:to_schema_element)
  | _ -> (
      match json with
      | `Assoc _ when nullish (keys json) -> `Null
      | `Null -> `Null
      | _ -> failwith @@ Yojson.Safe.to_string json)

and to_schema_element json : schema_element =
  let open Yojson.Safe.Util in
  let title = json |> member "title" |> to_string_option
  and description = json |> member "description" |> to_string_option
  and enum = json |> member "enum" |> to_string_list_option
  and kind = json |> to_schema_kind in
  { title; description; kind; enum }

and to_schema_array = function
  | `List els -> List.map ~f:to_schema_element els
  | json -> [ to_schema_element json ]

let to_definitions =
  List.fold ~init:(Dictionary.init ()) ~f:(fun dict (id, raw) ->
      let def = raw |> to_schema_element in
      Dictionary.(add_entry dict id def))

let to_json_schema json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  let toplevel = json |> member "$ref" |> to_string |> to_ident in
  let definitions =
    json |> member "definitions" |> to_assoc |> to_definitions
  in
  { toplevel; definitions }

let type_to_codec json =
  let open Yojson.Safe.Util in
  (* print_endline @@ Yojson.Safe.to_string json; *)
  let ident = json |> member "id" |> to_string in
  let _json = json |> member "json" |> to_json_schema in
  let binary = json |> member "binary" |> to_binary_spec in
  { ident; _json; binary }

let json_to_ast = function
  | `List types -> (
      match List.length types with
      | 0 -> raise Empty_input_json_array
      | 1 ->
          let codec = List.hd_exn types |> type_to_codec in
          Single codec
      | _ ->
          let codecs = List.map types ~f:type_to_codec in
          Many codecs)
  | `Assoc _ as typ ->
      let codec = type_to_codec typ in
      Single codec
  | invalid -> raise (Illegal_json_input invalid)
