#!/bin/bash

VERBOSE="false"
IDENT="*"

while getopts ":vi:" opt; do
    case ${opt} in
        v)
            VERBOSE="true"
            ;;
        i)
            IDENT=$OPTARG
            ;;
        /? ) echo "usage: whatchanged [-v] OLDVERSION NEWVERSION"
            ;;
    esac
done

shift $(($OPTIND - 1))

OLD="${1:-009-PsFLoren}"
NEW="${2:-010-PtGRANAD}"

main () {
    for i in $NEW.$IDENT.ts;
        do
            ID="${i#*.}"
            if [ -f "$OLD.$ID" ]
            then 
                case $NEW in
                    alpha)
                        cmp -s <(sed -E "s/(Proto)?${OLD%-*}(.)${OLD#*-}/$NEW/g" "$OLD.$ID") "$i" || echo "[whatchanged]: $OLD.$ID > $i"
                        if [ "$VERBOSE" == "true" ];
                            then diff -u -w <(sed -E "s/(Proto)?${OLD%-*}(.)${OLD#*-}/$NEW/g" "$OLD.$ID") "$i"
                        fi
                        ;;
                    *)
                        cmp -s <(sed -E "s/${OLD%-*}(.)${OLD#*-}/${NEW%-*}\\1${NEW#*-}/g" "$OLD.$ID") "$i" || echo "[whatchanged]: $OLD.$ID > $i"
                        if [ "$VERBOSE" == "true" ];
                            then diff -u -w <(sed -E "s/${OLD%-*}(.)${OLD#*-}/${NEW%-*}\\1${NEW#*-}/g" "$OLD.$ID") "$i"
                        fi
                        ;;
                esac
            else 
                echo "[whatchanged]: schema \"$ID\" was added between \"$OLD\"..\"$NEW\""
            fi
    done
    for i in $OLD.$IDENT.ts
        do
        if [ ! -f "$NEW.${i#*.}" ]
        then echo "[whatchanged]: schema \"<PROTO>.${i#*.}\" was removed between \"$OLD\"..\"$NEW\""
        fi
    done
}

cd $(find . -name 'compiled-typescript' -type d) && main