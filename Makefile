compiler-ml = abstract_type.ml ast_converter.ml codec_compiler.ml codec_repr.ml \
			 compiler_main.ml foreign_ast.ml foreign_converter.ml foreign_language.ml \
			 json_ast.ml language_repr.ml typescript.ml 

library: compiler paths
	dune exec tezos-codec-compiler test_input.json

build-deps:
	opam switch 4.10.2
	opam install tezos-codec-compiler

paths:
	mkdir -p langs/typescript/compiled-typescript/

compiler: $(compiler-ml)
	dune build